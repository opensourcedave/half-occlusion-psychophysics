rndSdTO=1;

trlPerLvl=1;
bRandomize=1;
bUseFeedback=0;
bSKIPSYNCTEST=1;
bDEBUG=0;
sizeMult=1;
timeMult=1;

subjName=input('Enter initials: ','s');
disp('Conditions:')
disp('  1. full image cues')
disp('  2. no image cues')
disp('  3. full image cues with foreground probe')

while true
    subExp=input('Enter condition number: ','s');
    switch subExp
    case '1'
      disp('100: full')
      fileParams_HOC_NAT_NAT2D_est_1_1;
    case '2'
      disp('010: none')
      fileParams_HOC_NAT_NAT2D_est_1_2;
    case '3'
      disp('110: full + fgnd prorbe')
      fileParams_HOC_NAT_NAT2D_est_1_3;
    otherwise
      disp('Incorrect response. Try again.')
      continue
    end
    break
end

while true
    pass=input('Enter pass number: ','s');
    try
        pass=str2double(pass);
        break
    catch
        disp('incorrect response. Try again.')
    end
end

%fileParams_HOC_NAT_NAT2D_est %DO NOT EDIT FOR THIS FILE

%LOAD P
fname = daVinciPatchFname(saveDTB,dspArcMin,minMaxPixDVN,fgndORbgnd,ctrORedg,PszXY,rndSd);
disp('Loading stimuli...')
P=load(fname);

%CREATE S
[S] = psyStimStructHalfOcc(P,trlPerLvl,0,0,rndSd,bRandomize,rndSdTO,S);
%GET SAVING INFO
[sfname,lfname]=depthProbeFname(S.D,S.P,S,pass,subjName);
%RUN EXPERIMENT
try
    [S,D,exitflag] = depthProbeAdjustRunHandler(S,subjName,S.indTrl,bUseFeedback,bSKIPSYNCTEST,bDEBUG,sizeMult,timeMult);
catch ME
    %SAVE VARIABLE TO WORKSPACE
    assignin('base','SS',S);

    %SAVE VARIALBE TO FILE
    disp('Saving to server...')
    save([sfname '_ERROR'],'S','-v7.3');
    disp('Saving locally...')
    save([lfname '_ERROR'],'S','-v7.3');

    sca;
    rethrow(ME)
end
assignin('base','SS',S);

%SAVE
disp('Saving to server...')
save(sfname,'S','-v7.3');
disp('Saving locally...')
save(lfname,'S','-v7.3');

scaa
%PLOT
depthProbeAdjustPlotResults(S);
