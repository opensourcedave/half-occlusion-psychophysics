function [PRB, IMG]=get_prb_img(s,prb,P,D,prbCrpLoc,bCropPrb,bRing)
    stmLfull=P.LccdRMS(:,:,s);
    stmRfull=P.RccdRMS(:,:,s);
    rmpSz=6;
    stmLprb=stmLfull;
    stmRprb=stmRfull;
    try
        stmLR=cropImageCtrInterp(stmLprb,     prbCrpLoc(1,:),D.pixPerDegXY.*D.prbXYdeg+rmpSz,'linear',0);
    catch
        stmLR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz))).*D.gry;
    end
    try
        stmRR=cropImageCtrInterp(stmRprb,     prbCrpLoc(2,:),D.pixPerDegXY.*D.prbXYdeg+rmpSz,'linear',0);
    catch
        stmRR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz))).*D.gry;
    end

    %CIRCULAR WINDOW -> ALPHA C
    sz=max(size(stmLR))-rmpSz;
    szs=[size(stmLR,2) size(stmLR,1)];
    %XXX
    W=cosWindowFlattop(szs,sz-5,rmpSz+5);

    prbR{1}(:,:,1)=stmLR;
    prbR{1}(:,:,2)=stmLR;
    prbR{1}(:,:,3)=stmLR;
    prbR{1}(:,:,4)=W;

    prbR{2}(:,:,1)=stmRR;
    prbR{2}(:,:,2)=stmRR;
    prbR{2}(:,:,3)=stmRR;
    prbR{2}(:,:,4)=W;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
    %% RING
    loc=prbCrpLoc(1,:);
    bFG = loc(2) > P.AitpRCfgnd(s,2)+D.prbRadius(1); %XXX 1?
    try
        if P.LorRall(s)=='L' && bFG
            bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
        elseif P.LorRall(s)=='L'
            bgnL=cropImageCtrInterp(double(P.LmonoBGmain(:,:,s) | P.LbinoFGmain(:,:,s)), loc,sz,'linear',0);
        elseif P.LorRall(s)=='R' && bFG
            bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
        elseif P.LorRall(s)=='R'
            bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
        end
    catch
        bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
    end

    loc=prbCrpLoc(2,:);
    bFG = loc(2) < P.AitpRCfgnd(s,2)+D.prbRadius(1);
    try
        if P.LorRall(s)=='L'  && bFG
            bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
        elseif P.LorRall(s)=='L'
            bgnR=zeros(round(fliplr(size(prb(:,:,1)))));

        elseif P.LorRall(s)=='R' && bFG
            bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
        elseif P.LorRall(s)=='R'
            bgnR=cropImageCtrInterp(double(P.RmonoBGmain(:,:,s) | P.RbinoFGmain(:,:,s)), loc,sz,'linear',0);
        end
    catch
        bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
    end
    %%%%%%%%%%%%%

    %HIDE RING (BY ALPHA) IF OCCLUDED
    prbIL=prb(:,:,4);
    prbIR=prb(:,:,4);
    %XXX round?
    prbIL(logical(ceil(bgnR)))=0;
    prbIR(logical(ceil(bgnL)))=0;

    prbI{1}(:,:,1)=prb(:,:,1);
    prbI{1}(:,:,2)=prb(:,:,2);
    prbI{1}(:,:,3)=prb(:,:,3);
    prbI{1}(:,:,4)=prbIL;

    prbI{2}(:,:,1)=prb(:,:,1);
    prbI{2}(:,:,2)=prb(:,:,2);
    prbI{2}(:,:,3)=prb(:,:,3);
    prbI{2}(:,:,4)=prbIR;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

    if P.LorRall(s)=='L'
        PRB=prbR{1};
        IMG=P.RccdRMS(:,:,s);
    elseif P.LorRall(s)=='R'
        PRB=prbR{2};
        IMG=P.LccdRMS(:,:,s);
    end

    if bCropPrb==1
        PRB=PRB(:,:,1).*PRB(:,:,4);
    else
        PRB=PRB(:,:,1);
    end
    PRB(1:3,:)=[];
    PRB(:,1:3)=[];
    PRB(:,end-1:end)=[];
    PRB(end-1:end,:)=[];

    if bRing==1
        if P.LorRall(s)=='L'
            RNG=prbI{1};
        elseif P.LorRall(s)=='R'
            RNG=prbI{2};
        end
        RNGA=(RNG(:,:,4));
        RNGA=imresize(RNGA,size(PRB),'bilinear');
        RNGA=RNGA/max(max(RNGA));

        RNGB=1-(RNG(:,:,4));
        RNGB=imresize(RNGB,size(PRB),'bilinear');
        RNGB=RNGB/max(max(RNGB));

        RNG=RNG(:,:,1).*RNG(:,:,4);
        RNG=imresize(RNG,size(PRB),'bilinear');
        RNG=RNG/max(max(RNG));

        PRB=(RNG.*RNGA)+(PRB.*(RNGB));
    end
end
