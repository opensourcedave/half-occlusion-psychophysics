function D = depthProbeAdjustPrbInit(P,D,s)
%LOCATION OF OCCLUSION PROBE IN _PATCH_ SPACE

switch D.prbLocType
case 'm1'
    D.prbLocDsp=0;
case 'm2'
    D.prbLocDsp=P.width(s)/4;
case 'm3'
    D.prbLocDsp=P.width(s)/2;
case 'm4'
    D.prbLocDsp=P.width(s)/4*3;
case 'm5'
    D.prbLocDsp=P.width(s);
case 'be'
    D.prbLocDsp         = -1*D.prbRadius*2;
case 'bc'
    if strcmp(P.LorRall(s),'L')
        D.prbLocDsp       = -1*(P.beginWidth(s)/2);
    elseif strcmp(P.LorRall(s),'R')
        D.prbLocDsp       = -1*(P.endWidth(s)/2);
    end
end
D.prbLocDspB=P.width(s)-D.prbLocDsp;
D.prbLocDspBorder=P.width(s);

%AitpRCbgnd - anchor ctrORedg
%OFFSET FROM CENTER
D.prbXYoffsetPix=[P.PszXY(1)/2-P.AitpRCbgnd(s,2) 0];
D.prbXYoffsetPixB=D.prbXYoffsetPix;
D.prbXYoffsetPixBorder=[0 0];
if strcmp(P.LorRall(s),'L')
    D.prbXYoffsetPix(1)      =D.prbXYoffsetPix(1)-D.prbLocDsp+1;
    D.prbXYoffsetPixB(1)     =D.prbXYoffsetPixB(1)-D.prbLocDspB+1;
    D.prbXYoffsetPixBorder(1)=D.prbXYoffsetPixBorder(1);
elseif strcmp(P.LorRall(s),'R')
    D.prbXYoffsetPix(1)      = D.prbXYoffsetPix(1)+D.prbLocDsp-1;
    D.prbXYoffsetPixB(1)     = D.prbXYoffsetPixB(1)+D.prbLocDspB-1;
    D.prbXYoffsetPixBorder(1)= D.prbXYoffsetPixBorder(1)-1;

end