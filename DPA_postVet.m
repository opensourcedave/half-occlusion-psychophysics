loadP
fileParams_DVN_NAT_NAT_Adj;
prbCurXYpix=P.PszXY/2;
bad=zeros(length(P.LorRall),1);
for s = 1:length(P.LorRall)
    depth=DPA_getDepth(s,P,D,[],[],prbCurXYpix,0,1)
    if depth > 1.022
        bad(s)=1;
    end
end
P.bad=P.bad | bad;
[hctrs,hcounts]=hist(P.imgNum(~P.bad),1:98)
bar(hcounts,hctrs)
