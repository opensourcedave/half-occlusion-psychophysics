function [prbXYZm]=depthProbeGetProbeXYZambig(D,depthTrue,prbPPTxyzM,LorR)

%GET L & R CORRESPONDING POINTS OF IMAGE PROBE
if LorR=='L'
    prbXYZm=intersectLinesFromPoints(D.LExyz,prbPPTxyzM,[-2 0 depthTrue],[2 0 depthTrue]);
elseif LorR=='R'
    prbXYZm=intersectLinesFromPoints(D.RExyz,prbPPTxyzM,[-2 0 depthTrue],[2 0 depthTrue]);
end
