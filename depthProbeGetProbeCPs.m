function [prbXYZm,LitpXY,RitpXY,vrgArcMin,vrsArcMin,elvArcMin]=depthProbeGetProbeCPs(prbLOS,D,dspArcMinC,depthC,depthTrueC,depthTrue,prbPPTxyzM,LorR,IPDm,prbTxyzM)
prbXYZm=[];
if ~isvar('IPDm')
    IPDm=LRSIcameraIPD;
end

if isvar('dspArcMinC')
    CppXYZpsy=[0, 0, D.PPxyz(1,3)];
    CppVrgDeg = vergenceFromRangeXYZVec('C',IPDm,CppXYZpsy); %vergence at focus (screen)
    vrgDeg=dspArcMinC/60+CppVrgDeg;
    depthC=(IPDm/2)/tand(vrgDeg/2); %ADJACENT FROM OPPOSITE AND TAN(theta)

    vrgArcMin=vrgDeg*60;
    vrsArcMin=0;
    elvArcMin=nan;
end

%HANDLE DIFFERENCE LINES OF SIGHT
if strcmp(prbLOS,'C')  % CYCLOPIAN LINE OF SIGHT
    [vrsArcmin,prbXYZm]=depthProbeGetProbeXYZbinoC(depthC,depthTrueC,depthTrue,prbTxyzM);
elseif strcmp(prbLOS,'NC') || strcmp(prbLOS,'A') %ANCHOR LINE OF SIGHT
    [vrsArcmin,prbXYZm]=depthProbeGetProbeXYZbinoA(D,depthC,depthTrueC,depthTrue,prbTxyzM,prbPPTxyzM,LorR,IPDm);%ASSIGN ANCHOR EYE COORDINATES
elseif strcmp(prbLOS,'F') %CARTESIAN Y LINE OF SIGHT
    [vrsArcMin,prbXYZm]=depthProbeGetProbeXYZbinoCart(D,depthC,depthTrueC,depthTrue,prbTxyzM); %ASSIGN ANCHOR EYE COORDINATES
elseif strcmp(prbLOS,'Naive')
    prbXYZm = [      0, 0, depthC];
elseif strcmp(prbLOS,'Ambig')
    [prbXYZm]=depthProbeGetProbeXYZambig(D,depthTrue,prbPPTxyzM,LorR);
    vrsArcMin=60*atand(prbPPTxyzM(1)/depthTrue);      %VERSION ...
end

%GET L & R CORRESPONDING POINTS OF RESPONSE PROBE
LitpXYZ=intersectLinesFromPoints(D.LExyz,prbXYZm,D.PPxyz(1,:),D.PPxyz(2,:));
RitpXYZ=intersectLinesFromPoints(D.RExyz,prbXYZm,D.PPxyz(1,:),D.PPxyz(2,:));
if ~isvar('prbLOS') && LorR=='L'
    RitpXYZ=prbPPTxyzM;
elseif ~isvar('prbLOS') && LorR=='R'
    LitpXYZ=prbPPTxyzM;
end

%XYZ CYCLOPIAN-SCREEN COORDINATES OF PROJECTION PLANE PROBE IN METERS
LitpXY=LitpXYZ(1:2).*D.pixPerMxy+D.scrnCtr;
RitpXY=RitpXYZ(1:2).*D.pixPerMxy+D.scrnCtr;

%hDist=RctrXYZm(1)*1000*D.pixPerMmXY(1);
%if isnan(hDist)
%    hDist=0;
%end
%LitpXY(1)=D.scrnCtr(1)-hDist;
%RitpXY(1)=D.scrnCtr(1)+hDist;

%STATS ALONG CYCLOPIAN
LS=xyz2dist(D.LExyz,prbXYZm);
RS=xyz2dist(D.RExyz,prbXYZm);
if ~isvar('vrgArcMin')
    vrgArcMin=60*acosd((LS^2+RS^2-(IPDm)^2)/(2*LS*RS)); %LAW OF COSINES -> VERGENCE
end
if ~isvar('dspArcMin')
    dspArcMin=vrgArcMin-D.vrgArcMinF;
end
if ~isvar('elvArcMin')
    elvArcMin=60*atand(prbPPTxyzM(2)/depthTrue);      %ELEVATION ...
end
