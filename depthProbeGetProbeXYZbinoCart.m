function [vrsArcMin,prbXYZm]=depthProbeGetProbeXYZbinoCart(D,depthC,depthTrueC,depthTrue,prbTxyzM)%ASSIGN ANCHOR EYE COORDINATES
    vrsArcMin=0;
    depth=depthC*depthTrue/depthTrueC;
    prbXYZm=[prbTxyzM(1) prbTxyzM(2) depth];
end
