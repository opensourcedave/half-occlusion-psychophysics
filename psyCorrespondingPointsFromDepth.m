function [LitpXYT,RitpXYT,prbTxyzM] = psyCorrespondingPointsFromDepth(depth,prbCurXYpix,LorR,PszXY,multFactorXY,pixPerMxy,scrnCtr,LExyz,RExyz,PPxyz)

if ~isvar('LExyz')
  LExyz  =[-0.065/2, 0, 0];
end
if ~isvar('RExyz')
  RExyz  =[0.065/2, 0, 0];
end
if ~isvar('PPxyz')
  PPxyz  =[-1, 0, 1;  1, 0, 1];
end

%XYZ CYCLOPIAN-SCREEN COORDINATES OF PROJECTION PLANE PROBE IN METERS
prbCurXYscrnM=(prbCurXYpix-PszXY/2).*multFactorXY./pixPerMxy;
prbPPTxyzM=[prbCurXYscrnM PPxyz(2,3)];

%GET L & R CORRESPONDING POINTS OF IMAGE PROBE
if LorR=='L'
  prbTxyzM=intersectLinesFromPoints(LExyz,prbPPTxyzM,[-2 0 depth],[2 0 depth]);
  LitpXYZ=prbPPTxyzM;
  RitpXYZ=intersectLinesFromPoints(RExyz,prbTxyzM,PPxyz(1,:),PPxyz(2,:));
elseif LorR=='R'
  prbTxyzM=intersectLinesFromPoints(RExyz,prbPPTxyzM,[-2 0 depth],[2 0 depth]);
  RitpXYZ=prbPPTxyzM;
  LitpXYZ=intersectLinesFromPoints(LExyz,prbTxyzM,PPxyz(1,:),PPxyz(2,:));
end

LitpXYT=LitpXYZ(1:2) .*pixPerMxy+scrnCtr; %CORRESPONDING POINTS IN IMAGE-SCREEN COORDINATES
RitpXYT=RitpXYZ(1:2) .*pixPerMxy+scrnCtr; %CORRESPONDING POINTS IN IMAGE-SCREEN COORDINATES
