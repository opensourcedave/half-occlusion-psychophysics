function [dspArcMinDisplay,TvrgDisp,FvrgDisp]=dspLRSI2dspDisplayNaive(dspArcMinLRSI,PszXY,stmXYdeg,hostName)
% example call:
%     [dspArcMinDisplay,TvrgDisp,FvrgDisp]=dspLRSI2dspDisplayNaive(-4,[157 53],[3*.79252 1*.7971],'jburge-wheatstone')
%CONVERTS DISPARITY OF CENTRAL PIXEL IN LRSI IMAGE TO DISPARITY OF CENTRAL PIXEL IN DISPLAY
%ASSUMES CENTRAL !!!

%ARCMIN2DEGREES
degreeLRSI=dspArcMinLRSI/60;

%EYE COORDINATES
IPDm=.065;
I=IPDm/2;
LExyz   = [-IPDm/2, 0, 0];
RExyz   = [IPDm/2,  0, 0];

%PROJECTION PLANES
[CppXm, CppYm, CppZm]     = LRSIprojPlaneAnchorEye('C');
[pdCppXm,pdCppYm,pdCppZm] = psyProjPlaneAnchorEye('C',1,1,PszXY,stmXYdeg,hostName);
PPxyz   = [-3, 0, CppZm;  3, 0, CppZm];
[X,Y]=meshgrid(1:size(CppXm,2),1:size(CppYm,1));

%DISPLAY PARAMS
D=psyPTBdisplayParameters([],hostName);

%FOCAL VERGENCE, ASSUMING Y=0 & VERSION=0
FvrgLRSI=2*atand(I/CppZm);
FvrgDisp=2*atand(I/pdCppZm);

%GET POINT, ASSUMING Y=0 & X=0
TvrgLRSI=degreeLRSI+FvrgLRSI; %d=T-F, T=D+F
depthLRSI=I/tand(TvrgLRSI/2);
pXYZm=[0 0 depthLRSI];

%GET CPS IN METERS FROM LRSI PLANE, ASSUMING Y=0
LitpXYZlrsiM=intersectLinesFromPoints(LExyz,pXYZm,PPxyz(1,:),PPxyz(2,:));
RitpXYZlrsiM=intersectLinesFromPoints(RExyz,pXYZm,PPxyz(1,:),PPxyz(2,:));

%GET CPS IN PIXELS
LitpRC(1)=reverseInterp1D(CppYm(:,1),LitpXYZlrsiM(2));
LitpRC(2)=reverseInterp1D(CppXm(1,:),LitpXYZlrsiM(1));
RitpRC(1)=reverseInterp1D(CppYm(:,1),RitpXYZlrsiM(2));
RitpRC(2)=reverseInterp1D(CppXm(1,:),RitpXYZlrsiM(1));

%GET POINT IN DISPLAY
[CitpXYZ] = LRSIcorrespondingPoint2sceneXYZ(LitpRC,RitpRC,pdCppXm,pdCppYm,pdCppZm,IPDm);
depthDisplay=CitpXYZ(:,3);

%GET DISPARITY, ASSSUMING Y=0 & VERSION=0
TvrgDisp=2*atand(I/(depthDisplay));
dspArcMinDisplay=60*(TvrgDisp-FvrgDisp);
