
%VARS
fileParams_DVN_NAT_NAT_Adj
fileParams_DVN_NAT_NAT_Adj_3d4 %WHICH EXPERIMENT
%D.prbType='arrow';

%PROCESS
dspArcMin=dspArcMinAll(dspArcMinInd,:);
minMaxPixDVN=minMaxPixDVNall(minMaxPixDVNind,:);

%LOAD
fname='/Volumes/Data/Project_Databases/LRSI/Patch/DVN/daVinciZonePatches_8d6bc09aacc3dd774258d5e603ce1288';
if ~isvar('P')
    P=load(fname);
end

%RUN
indTrl=1:size(P.LphtCrpAll,3);
[R,P,D,exitflag] = ExpDVNadjust(D,C,P,G,indTrl,0,0);
