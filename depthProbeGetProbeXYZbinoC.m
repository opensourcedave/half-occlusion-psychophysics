function [vrsArcmin,prbXYZm]=depthProbeGetProbeXYZbinoC(depthC,depthTrueC,depthTrue,prbPPTxyzM)
    %XYZ COORDINATES OF RESPONSE PROBE - FROM SIMLAR TRIANGLES
    prbXYZm(1)=depthC*prbPPTxyzM(1)/depthTrueC;
    prbXYZm(2)=prbPPTxyzM(2);
    prbXYZm(3)=depthC*depthTrue/depthTrueC; %prbXYZm = depth

end
