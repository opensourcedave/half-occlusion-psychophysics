% FIX DISPARITY CALCULATION FROM DEPTH
P=load('/Volumes/Data/Project_Databases/LRSI/Patch/DVN/daVinciZonePatches_8d6bc09aacc3dd774258d5e603ce1288');
%assignin('base','P',P);
[X,Y]  =meshgrid(1:P.PszXY(1),1:P.PszXY(2));

fname=cell(0);
fname{end+1}='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_001-0-0.mat';
%fname{end+1}='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_002-0-0.mat';
%fname{end+1}='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_003-0-0.mat';
%fname{end+1}='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_004-0-0.mat';
%fname{end+1}='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_005-0-0.mat';

bPlot=1;
bSave=0;
for i = 1:length(fname)
    fixdsp(fname{i},P,X,Y,bSave,bPlot)
end

function fixdsp(fname,P,X,Y,bSave,bPlot)
    IPDm=.065;
    S=load(fname);
    %assignin('base','S',S)
    %S=evalin('base','S');
    D=S.D;
    S.depthTrueXYZ=zeros(size(S.depthTrueC));
    S.dspArcMinTrueC=zeros(size(S.depthTrueC));
    S.dspArcMinC=zeros(size(S.depthTrueC));

    % XXX NEED TO VERIFY THAT ADDED DSP IS OK
    [dspArcMinDisplay,TvrgDisp,S.FvrgDisp]=dspLRSI2dspDisplayNaive(P.dspArcMinAll,P.PszXY,D.stmXYdeg,D.hostName);

    for t = 1:length(S.blksIndTrl)
        s=S.indTrl(t);
        P.depthTrueC(t)=S.depthTrueC(t);

        %VRG TRUE
        prbXYoffsetPix  = S.XhatLocXYpix(t,:);
        prbCurXYpix     = P.PszXY./2-prbXYoffsetPix;

        if strcmp(P.LorRall(s),'L')
            S.depthTrueXYZ(t)       = interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1),prbCurXYpix(2));
        elseif strcmp(P.LorRall(s),'R')
            S.depthTrueXYZ(t)       = interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1)-3-P.width(s),prbCurXYpix(2));
        end

        [~,~,prbTxyzM]=psyCorrespondingPointsFromDepth(S.depthTrueXYZ(t),prbCurXYpix,P.LorRall(s),P.PszXY,D.multFactorXY,D.pixPerMxy,D.scrnCtr,D.LExyz,D.RExyz,D.PPxyz);
        LS=xyz2dist(D.LExyz,prbTxyzM);
        RS=xyz2dist(D.RExyz,prbTxyzM);

        S.vrgArcMinTrueC(t)=60*acosd((LS^2+RS^2-IPDm^2)/(2*LS*RS));  %LAW OF COSINES -> Vergence
        S.dspArcMinTrueC(t)=S.vrgArcMinTrueC(t)-S.FvrgDisp;

        %VRG
        S.depth(t)=S.depthC(t)*S.depthTrueXYZ(t)/S.depthTrueC(t);
        rnd=D.trlSds(t)*D.prbRandWidth-D.prbRandWidth/2; %add random
        prbXYZm=[prbTxyzM(1) prbTxyzM(2) S.depth(t)];

        LitpXYZ=intersectLinesFromPoints(D.LExyz,prbXYZm,D.PPxyz(1,:),D.PPxyz(2,:));
        RitpXYZ=intersectLinesFromPoints(D.RExyz,prbXYZm,D.PPxyz(1,:),D.PPxyz(2,:));
        LitpXY=LitpXYZ(1:2).*D.pixPerMxy+D.scrnCtr;
        RitpXY=RitpXYZ(1:2).*D.pixPerMxy+D.scrnCtr;

        LS=xyz2dist(D.LExyz,prbXYZm);
        RS=xyz2dist(D.RExyz,prbXYZm);
        S.vrgArcMinC(t)=60*acosd((LS^2+RS^2-IPDm^2)/(2*LS*RS)); %LAW OF COSINES -> VERGENCE
        S.dspArcMinC(t)=S.vrgArcMinC(t)-S.FvrgDisp;

    end
    %VRG GEOM
    [S.Xstar,S.vrgArcMinGeomC]=depthProbeGeometricBatch(P,S.D,S.blksIndTrl);
    % XXX adjust dspArcMinAll
    S.dspArcMinGeomC=S.vrgArcMinGeomC-S.FvrgDisp;

    %%%%%%%%%%%%%%5
    if bPlot==1
        depthProbeAdjustPlotResults(S)
    end
    assignin('base','S',S);
    if bSave==1
        save(fname,'-struct','S','-v7.3');
    end
end

%fname='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/DNW/HOC_NAT_NAT__jburge-wheatstone_m3-010_Rnd1-1_DNW_PILOT_0.mat';
%load(fname)
%SS=S;
%clear S
%
%fname='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/DNW/HOC_NAT_NAT__jburge-wheatstone_m3-100_Rnd1-1_DNW_PILOT_0.mat';
%load(fname)
%S.P.Xxyz=SS.P.Xyz;
%save(fname,'S','-v7.3');
