flds=fieldnames(ST);
statNames=flds(~contains(flds,'_'));
ST.statNames=unique(cellfun(@(x) x(~ismember(x,alphaUpperV)),statNames,'UniformOutput',false));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [rmsBino,grdBino]=rmsZoneDouble(Limg,Rimg,Lmask,Rmask,bSeparate)
%RMS HANDLER
    L_kRMS=rmsZone(Limg,Lmask);
    R_kRMS=rmsZone(Rimg,Rmask);
    rmsBino=(L_kRMS+R_kRMS)/2;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[YbBG] = partFFT(Limg,P.LbinoBG(:,:,i),P.DC);
[YbFG] = partFFT(Limg,P.LbinoFG(:,:,i),P.DC);

function [A,P] = partFFT(img,mask)
    img=imgPart(img,mask);
    Y=fft2(img);
    A=abs(Y);
    P=angel(Y);

    figure(333)
    imagesc(fftshift(A))
    drawnow
    waitforbuttonpress
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% MEAN DC

%mean luminance
ST.gdcA        = zeros(size(P.LorRall));
ST.gdcM        = zeros(size(P.LorRall));
ST.gdcB        = zeros(size(P.LorRall));
ST.gdcF        = zeros(size(P.LorRall));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DIFFERENCE STATS RMS CONTRAST
ST=diffStats(ST,i,bNorm,ST.statNames);

ST.h_FB   = zeros(size(P.LorRall));
ST.h_FM   = zeros(size(P.LorRall));
ST.h_BM   = zeros(size(P.LorRall));
ST.e_FB   = zeros(size(P.LorRall));
ST.e_FM   = zeros(size(P.LorRall));
ST.e_BM   = zeros(size(P.LorRall));
ST.corr_FB   = zeros(size(P.LorRall));
ST.corr_FM   = zeros(size(P.LorRall));
ST.corr_BM   = zeros(size(P.LorRall));
ST.dc_FB    = zeros(size(P.LorRall));
ST.dc_FM    = zeros(size(P.LorRall));
ST.dc_BM    = zeros(size(P.LorRall));
ST.rms_FB   = zeros(size(P.LorRall));
ST.rms_FM   = zeros(size(P.LorRall));
ST.rms_BM   = zeros(size(P.LorRall));


ST.rms_FB(i)=diffStat(ST.rmsBinoFG(i),ST.rmsBinoBG(i),bNorm);
ST.rms_FM(i)=diffStat(ST.rmsBinoFG(i),ST.rmsMonoBG(i),bNorm);
ST.rms_BM(i)=diffStat(ST.rmsBinoBG(i),ST.rmsMonoBG(i),bNorm);

ST.dc_FB(i)=diffStat(ST.dcBinoFG(i),ST.dcBinoBG(i),bNorm);
ST.dc_FM(i)=diffStat(ST.dcBinoFG(i),ST.dcMonoBG(i),bNorm);
ST.dc_BM(i)=diffStat(ST.dcBinoBG(i),ST.dcMonoBG(i),bNorm);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ST=diffStats(ST,i,bNorm,stats)

    for j = 1:length(stats)
        stat=stats{j};
%[stat 'F']
        F= ST.([stat 'F'])(i);
        B= ST.([stat 'B'])(i);
        M= ST.([stat 'M'])(i);

        ST.([stat '_FB'])(i)=diffStat(F,B,bNorm);
        ST.([stat '_FM'])(i)=diffStat(F,M,bNorm);
        ST.([stat '_BM'])(i)=diffStat(B,M,bNorm);
    end
end

function C=diffStat(A,B,bNorm)
    if bNorm
        C=(A-B)./((A+B)./2);
    else
        C=(A-B);
    end
end
