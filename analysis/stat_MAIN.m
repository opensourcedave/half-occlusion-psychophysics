bSave=1;
fdir     = '/Volumes/Data/Project_PsyData/DVN/NAT/NAT/';
fnameStat = [fdir 'STATcomplete'];

Opts.bBasic    = 1;
Opts.bGLCM     = 1;
Opts.bXcorr    = 1;
Opts.bSlope    = 1;
Opts.bDiff     = 1;

Opts.bPlotInd     = 1;
Opts.bCheck       = 1;

%DIFF
Opts.bDiffNorm = 1;

%BASIC
BasicOpts.tPlot     = 0;
BasicOpts.bSeparate = 0;

%GLCM
GlcmOpts.version   = 'mat'; %mat, ext, vec
GlcmOpts.bSeparate = 0;
GlcmOpts.bNorm     = 0;

%Xcorr
XcorrOpts.tPlot    = 0;
XcorrOpts.bRing    = 1;         %include ring in probe
XcorrOpts.bCropPrb = 0;      %crop probe so its a circle
XcorrOpts.bCropCX  = 1;       %crop correlelogram
XcorrOpts.weight   = 'euclid'; %

%SLOPE
SlopeOpts.bPlotSurf=0;
SlopeOpts.bRemoveNan=0;
SlopeOpts.norma='on';
SlopeOpts.bSmear=1;
%SlopeOpts.TYPE={'poly11','nearest'};
%SlopeOpts.TYPE={'nearest'};
SlopeOpts.TYPE={};
%opt.poly11
%0.068


% ------------------------------------------------------------
Opts.BasicOpts = BasicOpts;
Opts.GlcmOpts  = GlcmOpts;
Opts.XcorrOpts = XcorrOpts;
Opts.SlopeOpts = SlopeOpts;

if ~isvar('P')
    loadP
end
if ~isvar('Sp')
    loadSp
end
if ~isvar('D')
    loadD
end
G=Sp.G;

SST=cell(2,3);
subs=distribute(1:2,1:3);
for s=1:size(subs,1)
    sub=subs(s,:);
    e=sub(1);
    se=sub(2);

    %CREATE MASKS
    if     e==1  && se==1
        MASKL=repmat(ones(fliplr(P.PszXY)),1,1,numel(P.LorRall));
        MASKR=repmat(ones(fliplr(P.PszXY)),1,1,numel(P.LorRall));
    elseif e==1  && se==2
        MASKL=repmat(zeros(fliplr(P.PszXY)),1,1,numel(P.LorRall));
        MASKR=repmat(zeros(fliplr(P.PszXY)),1,1,numel(P.LorRall));
    elseif e==2  && se==1
        MASKL=~P.LmonoBGmain;
        MASKR=~P.RmonoBGmain;
    elseif e==2  && se==2
        MASKL=P.LmonoBGmain | P.LbinoFGmain;
        MASKR=P.RmonoBGmain | P.RbinoFGmain;
    elseif e==2  && se==3
        MASKL=P.LbinoFGmain;
        MASKR=P.RbinoFGmain;
    else
        continue
    end

    %CREATE STIMULI USING MASKS
    Ptmp=P;
    Ptmp.LccdRMS=MASKL.*P.LccdRMS + ~MASKL*P.DC;
    Ptmp.RccdRMS=MASKR.*P.RccdRMS + ~MASKR*P.DC;

    if Opts.bPlotInd
        imagesc([Ptmp.LccdRMS(:,:,1) Ptmp.RccdRMS(:,:,1)])
        title([num2str(e) '-' num2str(se)])
        formatImage;
        drawnow
    end

    %GET STATS PER MASK
    SST{e,se}=stat_patch(Ptmp,D,G,Opts);
    SST{e,se}.nums=sub;

end

STp=cellStructMerge(SST,1);
STp.fname=fnameStat;

if bSave
    save(fnameStat,'-v7.3','STp')
    disp('Saved STp')
end

Chk=struct;
if Opts.bCheck
    flds=fieldnames(STp);
    for i = 1:length(flds)
        fld=flds{i};
        if iscell(STp.(fld))
            Chk.(fld)=cellSimilarity(STp.(fld));
        end
    end
end

%======================================================================
%FUNCTIONS

function ST=stat_patch(P,D,G,Opts)
    ST=struct;

    %BASIC
    if Opts.bBasic
        ST=stat_basic(P,Opts.BasicOpts);
    end

    %GLCM
    if Opts.bGLCM
        OUT=stat_GLCM(P,Opts.GlcmOpts);
        ST=adoptFields(ST,OUT);
    end

    %Xcorr
    if Opts.bXcorr
        OUT=stat_XcorrPatch(D,G,P,Opts.XcorrOpts);
        ST=adoptFields(ST,OUT);
    end

    %Xslope
    if Opts.bSlope
        [ST.R2fg,ST.interpErr,ST.model] = stat_slope(P,Opts.SlopeOpts);
    end
    %DIFF
    if Opts.bDiff
        ST=stat_diff(ST,Opts.bDiffNorm);
    end
end
