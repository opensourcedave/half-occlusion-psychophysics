bSave=1;
bLoad=0;
modules=1:4;

%prjInd=[{[2]},{[1,2]}];
prjInd=[{[1:2]},{[1:3]}];

%SUBJ={'THD'};
%SUBJ={'JDB'};
SUBJ={'DNW','JDB','THD'};

passes=1:2;

prjHier={'DVN','NAT','NAT'};
expMethod='Adj';
indName='blksIndTrl';
expType='';
newInd=[8,9];
B=1:9;
gdSubj='DNW';
fromGd={'depthTrueC'}; %fields to grab from a good example

fdir     = '/Volumes/Data/Project_PsyData/DVN/NAT/NAT/';
fnameSall= [fdir 'SallComplete'];
fnameSp  = [fdir 'SPcomplete'];
fnameSS  = [fdir 'SScomplete'];

fldNames={...
             'errorT' ...
             'errorG' ...
         };
fncNames={...
             'error1' ...
             'error2' ...
         };

%----------------------------------------------------------------------
if ~isvar('P')
    P=[];
end

%1 SALL
if ismember(1,modules)
    Sall=blocks2cellStruct(SUBJ,prjHier,prjInd,expType,B,newInd,gdSubj,fromGd,indName,expMethod,passes,fnameSall,bSave);
elseif ismember(2,modules) & bLoad
    Sall=load(fnameSall);
    Sall=Sall.Sall;
end

%2 CELL STRUCT
if ismember(2,modules)
    SS=blockCombineAll(Sall,SUBJ,prjInd,B,passes,fnameSS,bSave);
elseif ismember(3,modules) & bLoad
    SS=load(fnameSS);
    SS=SS.SS;
end

if ismember(3,modules)
    SS     = add_params(SS,fldNames,fncNames);
    SS     = parse_error(SS);
    SS     = fix_star(SS,P);
    %SS     = fix_dsp(SS,P); %XXX
    %SS     = mark_geom_flags(SS);
    %FLAG   = combine_flags_exp(SS,prjInd,passes);
    disp('SS modifications done.')
end

if ismember(4,modules)
    Sp=cellStruct2special(SS,prjHier,expType,prjInd,passes,SUBJ,fnameSp,bSave);
end

if bSave
    save(fnameSS,'-v7.3','SS')
    disp('save successfull')
end

%----------------------------------------------------------------------

function SS = add_params(SS,newFldNames,fncNames)
    for e = 1:numel(SS)
    for l = 1:length(newFldNames)
        if isempty(SS{e})
            continue
        end
        SS{e}.(newFldNames{l})=feval(fncNames{l},SS{e});
    end
    end
end

function SS = parse_error(SS)
    for  e = 1:numel(SS)
        if isempty(SS{e})
            continue
        end
        flag=(abs(SS{e}.errorT) > 1);
        ind=SS{e}.flag == 2 | flag;
        SS{e}.flag(ind)=2;
    end
end

function SS = fix_star(SS,P)
    if ~isvar('P')
        P=load('/Volumes/Data/Project_Databases/LRSI/Patch/DVN/daVinciZonePatches_8d6bc09aacc3dd774258d5e603ce1288');
        assignin('base','P',P)
    end

    P.depthTrueC=SS{1,1,1}.depthTrueC;
    [Xstar, XstarFit]=depthProbeGeometricBatch(P,SS{1,1,1}.D,SS{1,1,1}.blksIndTrl);
    for e = 1:numel(SS)
        if isempty(SS{e})
            continue
        end
        SS{e}.Xstar   =Xstar;
        SS{e}.XstarFit=XstarFit;
    end
end
function [SS,FLAG]=combine_flags_exp(SS,prjInd,passes)

    subsAll=distributeCell(prjInd);
    for n = 1:numel(SS)
        if isempty(SS{n})
            continue
        end
        subs=ind2subAmbig(size(SS),n);
        str=[ 'SS{' num2strSane(subs) '}.flag;'];
        Flag=eval(str);
        Flag(isnan(Flag))=1;
        flag=Flag==2;
        mark=Flag==3;

        if numel(passes)==1 && ~isvar('FLAG')
            sz=size(SS);
            FLAG=logical(zeros([length(flag),sz(2:end),]));
            MARK=FLAG;
        elseif ~isvar('FLAG')
            FLAG=logical(zeros([length(flag),sz(2:end-1)]));
            MARK=FLAG;
        end

        if numel(passes)==1
            fsubs=num2strSane(subs(2:end));
        else
            fsubs=num2strSane(subs(2:end-1));
        end
        str=['FLAG(:,' fsubs ') | flag;' ];
        try
            tmp=eval(str);
        catch ME
            continue
        end
        eval(['Flag(:,' fsubs ') = tmp;']);

        str=['MARK(:,' fsubs ') | mark;' ];
        tmp=eval(str);
        eval(['MARK(:,' fsubs ') = tmp;']);

    end
end

function SS=mark_geom_flags(SS,FLAG)
    for i = 1:numel(SS)
        if ~isempty(SS{i})
            break
        end
    end

    x1=1.0146; %Foreground edge depth
    y1=x1;
    m=.5320;   %fitted slope
    b=y1-m*x1;
    linfunc=@(x) m*x + b;
    dT=SS{i}.depthTrueC;
    dGfit=linfunc(dT);
    errG=SS{i}.Xstar-dGfit;
    flag=errG < -0.004 | errG > 0.002;
    for e=numel(SS)
        if isempty(SS)
            continue
        end
        SS{e}.flag = flag;
    end
end
