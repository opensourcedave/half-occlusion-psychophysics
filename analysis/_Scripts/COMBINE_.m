clearvars -except SS
fnames=cell(0,1);
for i = 1:7
  fname=['/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_00' num2str(i) '-0-0.mat']; titl='Full Cues';
  %fname=['/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-2_0_jburge-wheatstone_DNW_00' num2str(i) '-0-0.mat']; titl='No Cues';
  %fname=['/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_2-1_0_jburge-wheatstone_DNW_00' num2str(i) '-0-0.mat']; titl='No mono-background';
  %fname=['/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_2-2_0_jburge-wheatstone_DNW_00' num2str(i) '-0-0.mat']; titl='No bino-background';
  %fname=['/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_2-3_0_jburge-wheatstone_DNW_00' num2str(i) '-0-0.mat']; titl='Foreground Only';

  if exist(fname,'file')==2
    fnames{end+1}=fname;
  end
end


fnameNew=nameCombine(fnames);
S=blockCombine(fnames);
depthProbeAdjustPlotResults(S,titl);
