function depth=DPA_getDepth(s,P,D,X,Y,prbCurXYpix,bSkipMap,bForce)

if ~isvar('X')
    [X,Y]  =meshgrid(1:P.PszXY(1),1:P.PszXY(2));
end
if ~isvar('D')
    D=[];
end
if ~isfield(D,'scrnZmm')
    D=setupPsychToolboxNew(D,1,'jburge-wheatstone')
elseif ~isfield(D,'CppXm')
    [D.CppXm, D.CppYm, D.CppZm] =  psyProjPlaneAnchorEye('C',1, 1,D.scrnXYmm./1000,D.scrnZmm./1000);
end


if ~isvar('bSkipMap')
    bSkipMap=0;
end
if ~isvar('bForce')
    bForce=0;
end

if (~bSkipMap && ~P.bPreDispDsp) || bForce
    [~,~,tmp1,~,~,tmp3,tmp4]=...
    disparityMapDisplay( P.AitpRC(:,:,s),P.ABitpRC(:,:,s),P.BitpRC(:,:,s),P.LorRall(s), D.stmXYdeg,P.PszXY,D.pdCppXm,D.pdCppYm,D.pdCppZm);

    try
        P.depthImgDisp(:,:,s)=tmp1;
        P.Xmap(:,:,s)=tmp3;
        P.Ymap(:,:,s)=tmp4;
    end
end

% DEPTH OF IMAGE PROBE
try
    if strcmp(P.LorRall(s),'L')
        depth= interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1),prbCurXYpix(2));
    elseif strcmp(P.LorRall(s),'R')
        depth= interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1)-3-P.width(s),prbCurXYpix(2));
    end
catch
    depth= 0;
end
