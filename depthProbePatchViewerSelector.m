function obj=depthProbePatchViewerSelector(obj,sub,viewer,quad,bPosition,P,V)
%depthProbePatchViewerSelector(obj,[1 1 1],'new',0,1,P)
% sub
% e - experiment number
% viewer - old, exp, new, both
% quad - 0 to 6; 0 = all, 1-4 = quadrants, 5-6 = confidence intervals
% bPosition - adjust figure position
% XXX

broken=[115 209 257 316 385 413 439 487 573 676 681];
long=[220 221 225 257 545 546 587 685];
bEdit=1;
bSave=1;

if ~isvar('sub')
    sub=[1 1 1];
end
% XXX
%if ~isvar('e')
%    e=1;
%end
if ~isvar('viewer')
    viewer='new';
end
if ~isvar('quad')
    quad=0;
end
if ~isvar('wallORcross')
    wallORcross='wall';
end
if ~isvar('bPosition')
    bPosition=1;
end


% XXX Ind=inds{e};
try
    inds=obj.select_inds(sub);
    Ind=select_quad_ind(obj,inds,quad);
catch
    Ind=[];
end

%LOAD DATA
D=obj.loadD(sub(2:end));
P=obj.loadP;
%SS=obj.select(sub);
%S=SS.Data;

%fileParams_DVN_NAT_NAT_Adj_1d1
if strcmp(viewer,'exp')
    ExpDVNadjust(D,C,P,G,Ind.ind,0,1)
    return
elseif strcmp(viewer,'old')
    Pnew=structSelect(P,691,Ind.ind);
    [gdInd]=daVinciPatchViewer(Pnew,'wall');
    return
elseif ~strcmp(viewer,'new')
  depthProbePatchViewer(P,D,t,s,Ind,prbCurXYpix,bPosition,wallORcross)
end

D=setupPsychToolboxNew(D,1,'jburge-wheatstone');
D.multFactorXY   = D.stmXYdeg.*D.pixPerDegXY./P.PszXY;

if isfield(obj.Data,'bViewed')
    t=find(obj.Data.bViewed,1,'last');
    if isempty(t)
        t=1;
    end
else
    t=1;
end
if isvar('Ind')
    Ind.Subjs{1,end+1}='All';
end
if isvar('Ind')
    N=length(Ind.ind);
    INDS=Ind.ind;
else
    N=length(obj.Data.(obj.indFld));
    INDS=1:N;
end
[obj]=save_init(obj,bSave);
init=1;
while true
    tlast=t;
    if t>N
        break
    end
    if ismember(t,broken) || ismember(t,long)
        t=t+1;
        continue
    end

    s=INDS(t);
    if init==1
        [D,prbCurXYpix,~]=depthProbeCurPix(P,D,s,1);
        V=get_V(obj,t);
    end

    [t,V,exitflag]=depthProbePatchViewer(P,D,t,s,Ind,prbCurXYpix,bPosition,wallORcross,bEdit,V,init);
    if exitflag==1
        break
    end

    [exitflag,t]=key_fun(t,bEdit);
    if t<1
        t=1;
    end
    if t<numel(s)
        t=numel(s);
    end

    if tlast~=t
        obj.Data.bViewed(tlast)=1;
        init=1;
        obj=update_fun(obj,V,tlast);
    else
        init=0;
    end
    if exitflag==1
        break
    end

end
out=basicYN('Save?');
if out==1
    obj.save;
end
end
%%%%%%%%%%%%%%%%%%%
function V = get_V(obj,t)
    if obj.Data.bViewed(t)==0
        V=struct;
    else
        V.EG=obj.Data.EG(t,:);
        V.FG=obj.Data.FG(t,:);
        V.BG=obj.Data.BG(t,:);
        V.EGind=obj.Data.EGind(t);
        V.FGind=obj.Data.FGind(t);
        V.BGind=obj.Data.BGind(t);
        V.prbCurXYpixSet=obj.Data.prbCurXYpixSet(t,:);
        V.prbTxyzM=obj.Data.prbTxyzM(t,:);
    end
end

function [obj]=save_init(obj,bSave)
    if bSave
    if ~isfield(obj.Data,'EG')
        N=length(obj.Data.(obj.indFld));
        obj.Data.EG=zeros(N,2);
        obj.Data.FG=zeros(N,2);
        obj.Data.BG=zeros(N,2);
        obj.Data.EGind=zeros(N,1);
        obj.Data.FGind=zeros(N,1);
        obj.Data.BGind=zeros(N,1);
        obj.Data.prbCurXYpix=zeros(N,2);
        obj.Data.prbTxyzM=zeros(N,3);
        obj.Data.Xold=obj.Data.X;
        obj.Data.depthTrueCold=obj.Data.depthTrueC;
        obj.Data.X=zeros(N,1);
        obj.Data.depthTrueC=zeros(N,1);
        obj.Data.bViewed=zeros(N,1);
    end
    end
end
function obj=update_fun(obj,V,t)
    % XXX verify correct ind
    obj.Data.EG(t,:)=V.EG;
    obj.Data.FG(t,:)=V.FG;
    obj.Data.BG(t,:)=V.BG;
    obj.Data.EGind(t)=V.EGind;
    obj.Data.FGind(t)=V.FGind;
    obj.Data.BGind(t)=V.BGind;

    obj.Data.prbTxyzM(t,:)=V.prbTxyzM;
    obj.Data.prbCurXYpix(t,:)=V.prbCurXYpix;
    obj.Data.X(t)=V.prbTxyzM(3); %reinit
    obj.Data.depthTrueC(t)=obj.Data.X(t);
end

function [exitflag,t]= key_fun(t,bEdit)
    keys=basicKeys;
    exitflag=0;
    if ~bEdit
        while true
            waitforbuttonpress
            k = double(get(gcf,'CurrentCharacter'));
            try
                switch k
                    case {keys.Left,keys.h}
                        t=t-1;
                    case {keys.Right,keys.l}
                        t=t+1;
                    case keys.Esc
                        exitflag=1;
                    otherwise
                        continue
                end
                return
            end
        end
    end
end
