clearvars -except P
if ~isvar('P')
    loadP
end
for s = 1:length(P.LorRall)
    LorR=P.LorRall(s);
    if LorR=='L'
        mono=P.LmonoBGmain(:,:,s);
        startAll=sum(cumprod(~mono,2),2)+1;
        sc=max(startAll);
        Io=P.LccdRMS(:,:,s);

        I1=Io;
        I1(~logical(P.LmonoBGmain(:,:,s)))=0;
        I1=I1(:,sc:end);
        I1=[fliplr(I1) I1];

        I2=Io;
        I2(logical(P.LmonoBGmain(:,:,s)))=0;
        I2=I2(:,sc:end);
        I2=[fliplr(flipud(I2)) I2];
        I2(~I1 & ~I2)=fliplr((I2(I1 & I2)));
        I1(I1 & I2)=0;

        imgL=I2+I1;

        bino=P.RbinoFGmain(:,:,s);
        startAll=sum(cumprod(~bino,2),2)+1;
        dist=floor((max(startAll)-min(startAll))/2);
        sc=min(startAll);
        Io=P.RccdRMS(:,:,s);
        Io(~logical(bino))=0;
        Io=Io(:,sc+dist:end);
        I1=[fliplr(flipud(Io)) zeros(size(Io,1),size(Io,2)-dist)];
        I2=[zeros(size(Io,1),size(Io,2)-dist) Io];
        I2(I1 & I2)=0;
        imgR=[I1 + I2];
        imgR(~imgR)=nan;
    elseif LorR=='R'
        continue
        mono=P.RmonoBGmain(:,:,s);
    end

    figure(1)
    imagesc(imgL.^.4)
    formatImage;
    drawnow;

    figure(2)
    imagesc(P.LccdRMS(:,:,s).^.4)
    formatImage;
    drawnow;
    waitforbuttonpress;

    figure(3)
    imagesc(imgR.^.4)
    formatImage;
    drawnow;

end
