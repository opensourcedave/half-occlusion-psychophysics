function [vrsArcmin,prbXYZm]=depthProbeGetProbeXYZbinoA(D,depthC,depthTrueC,depthTrue,prbTxyzM,prbPPTxyzM,LorR,IPDm)%ASSIGN ANCHOR EYE COORDINATES
    if ~isvar(IPDm)
        IPDm=LRSIcameraIPD;
    end

    if strcmp(LorR,'L')
        Axyz=D.LExyz;
    elseif strcmp(P.LorRall(s),'R')
        Axyz=D.RExyz;
    end

    %GET ANCHOR EYE VERSION
    prbCurXYpixA=abs(Axyz(1)-prbPPTxyzM(1));
    vrsArcMin=60*atand(prbCurXYpixA/depthTrue);

    %XYZ COORDINATES OF RESPONSE PROBE
    theta=90-vrsArcMin/60;
    prbXYZm=[sind(vrsArcMin/60*depthC), prbTxyzM(2), prbTxyzM(3)*depthC/depthTrueC];

    if strcmp(LorR,'L')
        Xi=prbTxyzM(1)+IPDm/2;
        prbXYZm(1)=depthC*Xi/depthTrueC-IPDm/2;
    elseif strcmp(LorR,'R')
        Xi=prbTxyzM(1)-IPDm/2;
        prbXYZm(1)=depthC*Xi/depthTrueC+IPDm/2;
    end
    prbXYZm(2)=prbTxyzM(2);
    prbXYZm(3)=depthC*prbTxyzM(3)/depthTrueC;
end
