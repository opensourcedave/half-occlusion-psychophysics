function [R,P,D,exitflag] = ExpDVNadjust(D,C,P,G,indTrl,bParent,bTst,Ind)
 %depthTrue  - CARTESIAN DEPTH
 %depthTrueC - LINE OF SIGHT DEPTH

if isvar('Ind')
    D.lock=1;
    D.subjInd=1;
    D.Subjs=Ind.Subjs;
    D.Subjs{end+1}='True';
else
    Ind=[];
    D.lock=0;
end

% HANDLE VARIABLES
if ~isvar('bTst')
    D.bTst=0;
else
    D.bTst=bTst;
end

if D.bTst==1
    D.bInfo=1;
else
    D.bInfo=0;
end


if ~exist('bParent','var') || isempty(bParent)
    bParent=0;
end

if ~isfield(D,'secSep')
    D.secSep=0;
end

key=PTBkey(1,0.2);

%INIT PRJPLANE
P.IPDm=LRSIcameraIPD;
[~,~,D.vrgArcMinF]=dspLRSI2dspDisplayNaive(P.dspArcMinAll,P.PszXY,D.stmXYdeg,D.hostName);

%INIT RESPONSE INFO
R=initR(indTrl);

%Generate random seeds for each trial
rng(D.rndSd);

%INIT MAPS IF NECESSARY
if P.bPreDispDsp==0
    P.depthImgDisp      = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
    P.Xmap              = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
    P.Ymap              = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
end

%SETUP PSYCHTOOLBOX
D=setupPsychToolbox(D);
Pctr=P.PszXY./2;

%GRID FOR LOCATION INTERPOLATION
[X,Y]  =meshgrid(1:P.PszXY(1),1:P.PszXY(2));

%INIT/MAKE TEXTURES
[prb,prbR,prbL,Lratio]=depthProbeGen(D,G);

%INIT POLY
D=initStimPoly(D);
D.prbPlySqrPix=initPrbPoly(D,D.prbXYdeg);
D.prbLXYdeg=D.prbXYdeg;
D.prbLXYdeg(2)=D.prbLXYdeg(2)*Lratio;
D.prbLPlySqrPix=initPrbPoly(D,D.prbLXYdeg);

%INITIALIZE PROJECTION PLANE FOR CURRENT DISPLAY
[D.pdCppXm,D.pdCppYm,D.pdCppZm] = psyProjPlaneAnchorEye('C',1,1,P.PszXY,D.stmXYdeg,D.hostName);
D.pdLppXm=D.pdCppXm+P.IPDm/2;
D.pdRppXm=D.pdCppXm-P.IPDm/2;

%WINDOW
W  = ones(size(P.LphtCrpAll(:,:,1)));

%%%%%%%%%%%%%%%
% RUN PTB LOOPS
t=0;
exitflag=0;
D.trlSds=rand(length(indTrl),1);
while t < length(indTrl)
    t=t+1;
    s=indTrl(t);
    redrawflag=1;
    startflag=1;
    D.bUserInput=0;
    switch D.stmMskMode
        case {1,4,6}
           if D.bRspRingReset
               D.bRspRing=0;
           end
        otherwise
               D.bRspRing=1;
    end
    while redrawflag==1
        %FIX CONTRAST IN PATCHES
        if D.bFixContrast==1
            P = patches2FixedContrastIntensityImages(s,P,D.DC,D.RMS,W,0,'bino');
        end

        %CREATE STIM RECT
        % XXX ?
        if ~isfield(D,'wdwXYpix')
            D.wdwXYpix=[0 0 1920 1080];
        end

        %HOW LARGE STIMULUS HAS BEEN SHRUNK/ENDXPANDED
        D.multFactorXY   = D.stmXYdeg.*D.pixPerDegXY./P.PszXY;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% GET PROBE TRACKING POSITION

        % PROBE BOUNDS
        D.maxPrbIncXYdeg = (D.stmXYdeg.*D.pixPerDegXY-3.*D.multFactorXY)./D.pixPerDegXY./2;

        %UPDATE POLYGON
        D=initStimPoly(D);

        %PRB RADIUS
        D.prbRadius      = abs(D.prbPlySqrPix(3) - D.prbPlySqrPix(1))/2;
        D.prbLradiusY    = D.prbRadius*Lratio;

        %LOCATION OF PROBES
        [D,prbCurXYpix,prbCurXYpixB,prbCurXYpixBorder]=depthProbeCurPix(P,D,s,startflag);

        prbCurXYscrnM=(prbCurXYpix-P.PszXY/2).*D.multFactorXY./D.pixPerMxy;
        prbPPTxyzM=[prbCurXYscrnM D.PPxyz(2,3)];

        bDVNzone = depthProbeAdjustBdvn(P.LorRall(s),P.LdvnCrpAll(:,:,s),P.RdvnCrpAll(:,:,s),prbCurXYpix,X,Y); %WHETHER IN DVN ZONE

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% GET DEPTH OF PROBES

        depthTrue=DPA_getDepth(s,P,D,X,Y,prbCurXYpix); %IMAGE
        depthPrbB=DPA_getDepth(s,P,D,X,Y,prbCurXYpixB,1); %RESPONSE
        depthPrbBorder=DPA_getDepth(s,P,D,X,Y,prbCurXYpixBorder,1); %BORDER

        % SELECT STARTING DEPTH FOR RESPONSE PROBE IN NOT TESTING
        if isvar('Ind')
            tt=find(Ind.ind==s);
            if D.subjInd>length(Ind.Subjs)
                depthC=Ind.X(tt);
            else
                depthC=Ind.Xhat(tt,D.subjInd);
            end
        elseif startflag==1 && R.viewed(t)==0 && strcmp(D.startAbsOrRel,'Rel')
            %XXX
            depthC=depthTrueC+rand(1)-.5;
        elseif startflag==1 && R.viewed(t)==0 && strcmp(D.startAbsOrRel,'Abs')
            depthC=rand(1).*range(D.prbMinMaxSz)+min(D.prbMinMaxSz);
        elseif startflag==1 && R.viewed(t)~=0
            depthC=R.depthC(t);
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % GET PROBE DRAW LOCATIONS

        % BORDER PROBE  - INDEPENDENT OF LINE OF SIGHT
        [~,LitpXYBorder, RitpXYBorder]=depthProbeGetProbeCPs('Naive',D,P.dspArcMinAll);

        % IMAGE PROBE (GIVEN DEPTH) - INDEPENDENT OF LINE OF SIGHT
        [prbTxyzM,LitpXYT,RitpXYT,vrgArcMinTrueC,vrsArcMinTrue,elvArcMinTrue]=...
            depthProbeGetProbeCPs('Ambig',D,[],[],[],depthTrue,prbPPTxyzM,P.LorRall(s),P.IPDm);
        %depthTrueC=sqrt(prbTxyzM(1)^2+prbTxyzM(3)^2);
        depthTrueC=prbTxyzM(3);

        % GET RESPONSE PROBE DRAW LOCATIONS (GIVEN DEPTH) - DEPENDENT ON LINE OF SIGHT
        [~,LitpXY,RitpXY,vrgArcMinC,vrsArcMin,elvArcMin]=...
            depthProbeGetProbeCPs(D.prbTrace,D,[],depthC,depthTrueC,depthTrue,prbPPTxyzM,P.LorRall(s),P.IPDm,prbTxyzM);

        % XXX check
        dspArcMinC=vrgArcMinC-D.vrgArcMinF;
        dspArcMinTrueC=vrgArcMinTrueC-D.vrgArcMinF;
        depth=depthC*depthTrue/depthTrueC;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %% DRAW

        %% BACKGROUND
        [D,C]=depthProbeBgnd(t,D,C,P,startflag);

        %% TRIAL COUNTER
        psyDemoTrialCounter(t,length(indTrl),D,C);

        %% TEXTURE HANDLING & CREATION
        [stmTex,prbTex,prbTexR,prbTexB,prbTexL,prbRingTex]=depthProbeStimMask(s,D,C,P,LitpXYT,RitpXYT,prb,prbR,prbL);

        %% DRAW STIMULUS AND PROBES
        for i = 0:D.bStereo
            j=i+1;
            if j==1
                ItpXYT=LitpXYT;
                ItpXY =LitpXY;
                ItpXYB=LitpXYBorder;
            else
                ItpXYT=RitpXYT;
                ItpXY =RitpXY;
                ItpXYB=RitpXYBorder;
            end
            Screen('SelectStereoDrawBuffer', D.wdwPtr, i);

            %DRAW STIM
            depthProbeDrawStim(D,stmTex{j});

            %DRAW PROBES
            depthProbeDrawProbes(s,j,D,P,ItpXYT,ItpXY,ItpXYB,prbTex,prbTexR,prbTexB,prbTexL,prbRingTex,bDVNzone);
            %DRAW INFO
            % XXX
            depthProbeDrawInfo(t,s,D,C,P,R,D.bInfo,depth,depthC,depthTrue,depthTrueC,vrgArcMinC,vrgArcMinTrueC,vrsArcMin,elvArcMinTrue,prbCurXYpix,Ind);
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %HIDE ON MOVE & FLIP
        Screen('DrawingFinished', D.wdwPtr);
        if (D.prbHideOnMove==1 && D.bUserInput==0) || (D.prbTsec==0 && D.prbHideOnMove==0)
            Screen('Flip', D.wdwPtr,[],0);
        elseif (D.prbHideOnMove==0) || (D.bUserInput==1 && D.prbHideOnMove==1)
            Screen('Flip', D.wdwPtr,[],1);
            tic
            stmTex{1} = Screen('MakeTexture', D.wdwPtr, stmL,[],[],2);
            stmTex{2} = Screen('MakeTexture', D.wdwPtr, stmR,[],[],2);

            pause(D.prbTsec-toc)
            for i =0:D.bStereo
                j=i+1;
                Screen('SelectStereoDrawBuffer', D.wdwPtr, i);
                Screen('DrawTexture', D.wdwPtr, stmTex{j}, [], stmSqr);
            end
            Screen('Flip', D.wdwPtr,[],0);
            D.bUsePrb=0;
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if isvar('Ind')
            depthProbePatchViewer(P,D,s,s,Ind,prbCurXYpix,1);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % SAVE VARIALBES
        startflag=0;

        R.XhatLocXYpix(t,:)=D.prbXYoffsetPix;
        R.elvArcMinTrue(t) =elvArcMinTrue;
        R.depthC(t)        =depthC;
        R.vrgArcMinTrueC(t)=vrgArcMinTrueC;
        R.vrgArcMinC(t)    =vrgArcMinC;
        R.vrsArcMin(t)     =vrsArcMin;
        R.depthTrueC(t)    =depthTrueC;
        R.X(t)             =R.depthTrueC(t);
        R.Xhat(t)          =R.depthC(t);
        R.bRspRing(t)      =D.bRspRing;

        R.LorR(t)          =P.LorRall(s);
        R.depth(t)         =depth;
        R.depthTrue(t)     =depthTrue;
        R.dspArcMinC(t)    =dspArcMinC;
        R.dspArcMinTrueC(t)=dspArcMinTrueC;

        %GET CLOSEST ESTIMATE
        fgndZm  = interp2(X,Y,P.depthImgDisp(:,:,s),Pctr(1),Pctr(2)); %distance from subj to fgnd
        widthm  = P.width(s)*D.multFactorXY(1)/D.pixPerMxy(1);        %width of DVN in meters
        ewidthm = D.prbLocDsp*D.multFactorXY(1)/D.pixPerMxy(1);       %distance from outer edge to query point in DVN region
        [R.Xstar(t), R.vrgArcMinGeomC(t)] = depthProbeGeometricNew(depthTrueC,widthm,ewidthm,fgndZm,P.IPDm);
        R.dspArcMinGeomC(t)=R.vrgArcMinGeomC(t)-D.vrgArcMinF;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % HANDLE INPUTS
        [R,D,C,P,startflag,redrawflag,depthC,exitflag,t]= depthProbeKeys(key,R,D,C,P,startflag,redrawflag,depthC,exitflag,t,s,Ind,indTrl);

        if exitflag==1; break; end
    end %redraw loop
    if exitflag==1; break; end
end
try
  [~,indSort]    =sort(R.X);
  R.Xsort        =R.X(indSort);
  R.XhatSort     =R.Xhat(indSort);
  R.XstarSort    =R.Xstar(indSort);
end

disp('Exiting experiment...')
assignin('base','RR',R);
assignin('base','PP',P);
assignin('base','DD',D);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if bParent==0
    assignin('base','P',P);
    assignin('base','D',D);
    scaa;
    %depthProbeAdjustPlotResults(R)
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FUNCTIONS
end
function R=initR(indTrl)
    R.flag             =  ones(length(indTrl),1);
    R.LorR             = zeros(length(indTrl),1);
    R.X                = zeros(length(indTrl),1);
    R.XB               = zeros(length(indTrl),1);
    R.Xstar            = zeros(length(indTrl),1);
    R.Xhat             = zeros(length(indTrl),1);
    R.XhatLocXYpix     = zeros(length(indTrl),2);
    R.viewed           = zeros(length(indTrl),1);
    R.Xxyz             = zeros(length(indTrl),3);
    R.depth            = zeros(length(indTrl),1);
    R.depthTrue        = zeros(length(indTrl),1);
    R.depthC           = zeros(length(indTrl),1);
    R.depthTrueC       = zeros(length(indTrl),1);
    R.vrsArcMin        = zeros(length(indTrl),1);
    R.elvArcMinTrue    = zeros(length(indTrl),1);
    R.bRspRing         = zeros(length(indTrl),1);
    R.vrgArcMinC       = zeros(length(indTrl),1);
    R.vrgArcMinTrueC   = zeros(length(indTrl),1);
    R.vrgArcMinGeomC   = zeros(length(indTrl),1);
    R.dspArcMinC       = zeros(length(indTrl),1);
    R.dspArcMinTrueC   = zeros(length(indTrl),1);
    R.dspArcMinGeomC   = zeros(length(indTrl),1);
end

function [bDVNzone] = depthProbeAdjustBdvn(LorR,LdvnCrp,RdvnCrp,prbCurXYpix,X,Y)

    %% DETERMINE IF IN DVN ZONE
    if strcmp(LorR,'L')
        A=double((LdvnCrp>0));
        bDVNzone    = round(interp2(X,Y,A,prbCurXYpix(1),prbCurXYpix(2)));
    elseif strcmp(LorR,'R')
        A=double((RdvnCrp>0));
        bDVNzone    = round(interp2(X,Y,A,prbCurXYpix(1),prbCurXYpix(2)));
    end
end

function [R,D,C,P,startflag,redrawflag,depthC,exitflag,t]= depthProbeKeys(key,R,D,C,P,startflag,redrawflag,depthC,exitflag,t,s,Ind,indTrl)
    %up   - add depth
    %down - rm depth
    %tab - switch prob init location
    %Enter - toggle flags, +shift show/hide flags
    %Space/right - next stim
    %left - prev stim
    %escape - exit experiment
    %
    %1-7 switch masks
    %
    %q - DC up
    %w - DC down
    %e - RMS up
    %r - RMS down

    %t - toggle ring
    %y - toggle border probe
    %o - toggle probe
    %a - detphTueC
    %s - toggle hide anchor
    %d - mbgnd & bfgnd
    %g
    %z - -RMSmaskAdj
    %x - +RMSmaskAdj
    %c - toggle line of sight
    %v - toggle bCyc ???
    %m
    %

    %i - show/hide info
    %
    %p - larger stim
    %n - smaller stim
    %u - update mask, projection plane
    %
    %f
    %b
    %
    %h - move probe left
    %j - move probe down
    %k - move probe up
    %l - move probe right


    %INIT
    D.prbXYoffsetDeg=D.prbXYoffsetPix./D.pixPerDegXY;
    fFlags={'-','A','B','C'};

    while true
        key=key.refresh();
        shiftflag=key.shiftflag;
        K=key.K;

        %PARSE INPUT
        if isempty(key.keycode)
            continue
        end

        switch key.keycode
        case {K.z}
            if D.bTst==0; break; end
            if D.RMSmaskAdj-.005 >= 0
                D.RMSmaskAdj=D.RMSmaskAdj-.005;
            end
        case {K.x}
            if D.bTst==0; break; end
            if D.RMSmaskAdj+.005 <= 2
                D.RMSmaskAdj=D.RMSmaskAdj+.005;
            end
        case {K.q}
            if D.bTst==0; break; end
            if D.gry-.0025 >= 0
                D.gry=D.gry-.0025;
                D.DC=D.gry;
            end
        case {K.w}
            if D.bTst==0; break; end
            if D.gry+.0025 <= 1
                D.gry=D.gry+.0025;
                D.DC=D.gry;
            end
        case {K.e}
        if D.bTst==0; break; end
        if D.RMS-.0025 >= 0
            D.RMS=D.RMS-.0025;
        end
        case {K.r}
        if D.bTst==0; break; end
        if D.RMS+.0025 <= 1
            D.RMS=D.RMS+.0025;
        end
        case {K.a}
            if D.bTst==0; break; end
                depthC=depthTrueC;
        case {K.s}
            if D.bTst==0; break; end
            if D.bHideNanchor==1
                D.bHideNanchor=0;
            elseif D.bHideNanchor==0
                D.bHideNanchor=1;
            end
            pause(.2);
        case {K.g}
            ktmp=key;
            ktmp.type='integer';
            last='';
            while true
                pause(.2)
                ktmp=ktmp.read_until;
                if ~strcmp(last,ktmp.str) && ~isnumeric(ktmp.str)
                    fprintf('%s',ktmp.str(end))
                    last=ktmp.str;
                end
                if ktmp.exitflag==1
                    break
                end
            end
            tmp=ktmp.str;
            if isempty(tmp)
                continue
            end
            if tmp <= length(indTrl) && tmp > 2
                t=tmp-1;
            end
            R.viewed(t+1)=1;
            redrawflag=0;
            pause(.2);
        case {K.zero}
            if D.bTst==0; break; end
            D.stmMskMode=0;
            C.stmMskTex='gray';
            pause(.2)
        case {K.one}
            if D.bTst==0; break; end
            D.stmMskMode=1;
            C.stmMskTex='gray';
            pause(.2)
        case {K.two}
            if D.bTst==0; break; end
            D.stmMskMode=2;
            C.stmMskTex='gray';
            pause(.2)
        case {K.three}
            if D.bTst==0; break; end
            D.stmMskMode=3;
            C.stmMskTex='gray';
            pause(.2)
        case {K.four}
            if D.bTst==0; break; end
            D.stmMskMode=4;
            C.stmMskTex='gray';
            pause(.2)
        case {K.five}
            if D.bTst==0; break; end
            D.stmMskMode=5;
            C.stmMskTex='gray';
            pause(.2)
        case {K.six}
            if D.bTst==0; break; end
            D.stmMskMode=6;
            C.stmMskTex='gray';
            pause(.2)
        case {K.seven}
            if D.bTst==0; break; end
            D.stmMskMode=7;
            C.stmMskTex='black';
            pause(.2)
        case {K.Uarrow} % ADD DEPTH
            D.bUserInput=1;
            if isfield(D,'subjInd') && D.subjInd+1 <= length(D.Subjs)
                D.subjInd=D.subjInd+1;
            elseif isfield(D,'subjInd') && D.subjInd+1 > length(D.Subjs)
                D .subjInd=1;
            elseif shiftflag==1 && (depthC+D.prbIncSz*4)<D.prbMinMaxSz(2)
                depthC=depthC+D.prbIncSz*4;
            elseif (depthC-D.prbIncSz)<D.prbMinMaxSz(2)
                depthC=depthC+D.prbIncSz;
                %else
                %depthC=D.prbMinMaxSz(2);
            end
            pause(.01)
        case {K.Darrow} % SUBTRACT DEPTH
            D.bUserInput=1;
            if isfield(D,'subjInd') && D.subjInd-1 >= 1
                D.subjInd=D.subjInd-1;
            elseif isfield(D,'subjInd') && D.subjInd-1 < 1
                D .subjInd=length(D.Subjs);
            elseif shiftflag==1 && (depthC-D.prbIncSz*4)>D.prbMinMaxSz(1)
                depthC=depthC-D.prbIncSz*4;
            elseif (depthC+D.prbIncSz)>D.prbMinMaxSz(1)
                depthC=depthC-D.prbIncSz;
                %else
                %depthC=D.prbMinMaxSz(1);
            end
            pause(.01)
        case {K.d}
            if D.bTst==0; break; end
            if strcmp(D.tstMode,'mBgnd')
                D.tstMode='bFgnd';
            elseif strcmp(D.tstMode,'bFgnd')
                D.tstMode='mBgnd';
            end
            startflag=1;
            pause(.2);
        case {K.c}
            if D.bTst==0; break; end
            if D.prbTrace=='C'
                D.prbTrace='A';
            elseif D.prbTrace=='A'
                D.prbTrace='C';
            end
            pause(.2)
        case {K.v}
            if D.bTst==0; break; end
            if D.bCyc==1
                D.bCyc=0;
            elseif D.bCyc==0
                D.bCyc=1;
            end
            pause(.2)
        case {K.y}
            if D.bTst==0; break; end
            if D.bPrbB==1
                D.bPrbB=0;
            elseif D.bPrbB==0
                D.bPrbB=1;
            end
            pause(.2)
        case {K.n}
            if D.bTst==0; break; end
            [~,indMax]=max(D.stmXYdeg);
            [~,indMin]=min(D.stmXYdeg);
            newMax=D.stmXYdeg(indMax)+D.stmXYIncSz;
            newMin=round(newMax*D.stmXYdeg(indMin)/D.stmXYdeg(indMax),2);
            if newMax > D.stmXYminMax(2)
            else
                D.stmXYdeg(indMax)=newMax;
                D.stmXYdeg(indMin)=newMin;
                D.bMskShapeUpdate=1;
            end
        case {K.p}
            if D.bTst==0; break; end
            [~,indMax]=max(D.stmXYdeg);
            [~,indMin]=min(D.stmXYdeg);
            newMax=D.stmXYdeg(indMax)-D.stmXYIncSz;
            newMin=round(newMax*D.stmXYdeg(indMin)/D.stmXYdeg(indMax),2);
            if newMax < D.stmXYminMax(1)
            else
                D.stmXYdeg(indMax)=newMax;
                D.stmXYdeg(indMin)=newMin;
                D.bMskShapeUpdate=1;
            end
        case {K.t}
            if D.bRspRing==1
                D.bRspRing=0;
            elseif D.bRspRing==0
                D.bRspRing=1;
            end
            pause(.2)
        case {K.u}
            if D.bTst==0; break; end
            [D.pdCppXm,D.pdCppYm,D.pdCppZm] = psyProjPlaneAnchorEye('C',1,1,P.PszXY,D.stmXYdeg,D.hostName);
            D.pdLppXm=D.pdCppXm+P.IPDm/2;
            D.pdRppXm=D.pdCppXm-P.IPDm/2;
            startflag=1;
        case {K.enter}
            %if D.bTst==0; break; end
            if shiftflag==1
                if D.bShowFlags==1
                D.bShowFlags=0;
                elseif D.bShowFlags==0
                D.bShowFlags=1;
                end
            else
                if R.flag(t) == length(fFlags)
                    R.flag(t)=1;
                else
                    R.flag(t)=R.flag(t)+1;
                end
            end
            pause(.2);
        case {K.i}
            if D.bTst==0; break; end
            if D.bInfo == 1
                D.bInfo=0;
            elseif D.bInfo == 0
                D.bInfo=1;
            end
            pause(.2);
        case {K.o}
            if D.bTst==0; break; end
            if D.bUsePrb == 1
                D.bUsePrb=0;
            elseif D.bUsePrb == 0
                D.bUsePrb=1;
            end
            pause(.2);
        case {K.h} % MOVE PROBE LEFT
            if D.bTst==0; break; end
            if shiftflag==1 && D.prbXYoffsetDeg(1)+D.prbIncXYdeg(1) <= D.maxPrbIncXYdeg(1)
                D.prbXYoffsetDeg(1)=D.prbXYoffsetDeg(1)+D.prbIncXYdeg(1);
            elseif shiftflag==1
                D.prbXYoffsetDeg(1)=D.maxPrbIncXYdeg(1);
            elseif D.prbXYoffsetDeg(1)+D.prbIncXYdeg(1)/8 <= D.maxPrbIncXYdeg(1)
                D.prbXYoffsetDeg(1)=D.prbXYoffsetDeg(1)+D.prbIncXYdeg(1)/8;
            else
                D.prbXYoffsetDeg(1)=D.maxPrbIncXYdeg(1);
            end
        case {K.l} % MOVE PROBE LEFT
            if D.bTst==0; break; end
            if shiftflag==1 && D.prbXYoffsetDeg(1)-D.prbIncXYdeg(1) >= -1*D.maxPrbIncXYdeg(1)
                D.prbXYoffsetDeg(1)=D.prbXYoffsetDeg(1)-D.prbIncXYdeg(1);
            elseif shiftflag==1
                D.prbXYoffsetDeg(1)=-1*D.maxPrbIncXYdeg(1);
            elseif D.prbXYoffsetDeg(1)-D.prbIncXYdeg(1)/8 >= -1*D.maxPrbIncXYdeg(1)
                D.prbXYoffsetDeg(1)=D.prbXYoffsetDeg(1)-D.prbIncXYdeg(1)/8;
            else
                D.prbXYoffsetDeg(1)=-1*D.maxPrbIncXYdeg(1);
            end
        case {K.k} % MOVE PROBE UP
            if D.bTst==0; break; end
            if D.prbXYoffsetDeg(2)+D.prbIncXYdeg(2) > D.maxPrbIncXYdeg(2)
                D.prbXYoffsetDeg(2)=D.maxPrbIncXYdeg(2);
            else
                D.prbXYoffsetDeg(2)=D.prbXYoffsetDeg(2)+D.prbIncXYdeg(2);
            end
        case {K.j} % MOVE PROBE DOWN
            if D.bTst==0; break; end
            if D.prbXYoffsetDeg(2)-D.prbIncXYdeg(2) < -1*D.maxPrbIncXYdeg(2)
                D.prbXYoffsetDeg(2)=-1*D.maxPrbIncXYdeg(2);
            else
                D.prbXYoffsetDeg(2)=D.prbXYoffsetDeg(2)-D.prbIncXYdeg(2);
            end
        case {K.Larrow}      % RECORD RESPONSE
            if t>2
                t=t-2;
            else
                continue
            end
            redrawflag=0;
        case {K.space,K.Rarrow} % RECORD RESPONSE
            R.viewed(t)=1;
            redrawflag=0;
            pause(.2);
        case {K.tab}     % EXIT EXPERIMENT
            if D.bTst==0; break; end
            switch D.prbLocType
            case 'bc'
                D.prbLocType='be';
            case 'be'
                D.prbLocType='m1';
            case 'm1'
                D.prbLocType='m2';
            case 'm2'
                D.prbLocType='m3';
            case 'm3'
                D.prbLocType='m4';
            case 'm4'
                D.prbLocType='m5';
            case 'm5'
                D.prbLocType='bc';
            end
            startflag=1;
            pause(.2)
        case {K.escape}     % EXIT EXPERIMENT
            assignin('base','RR',R);
            assignin('base','PP',P);
            assignin('base','DD',D);
            exitflag=1;
            return
        otherwise
            continue
        end
        break
    end
end

function [stmTex,prbTex,prbTexR,prbTexB,prbTexL,prbRingTex]=depthProbeStimMask(s,D,C,P,LitpXYT,RitpXYT,prb,prbR,prbL)
    prbCrpLoc(1,:)=fliplr(LitpXYT-D.scrnCtr+P.PszXY/2);
    prbCrpLoc(2,:)=fliplr(RitpXYT-D.scrnCtr+P.PszXY/2);

    D.partialOcclude=0;
    D.gryMult=0.1;

    rmpSz=6;
    prbRingTex=[];

    stmLfull=P.LccdRMS(:,:,s);
    stmRfull=P.RccdRMS(:,:,s);

    LmonoBG=P.LmonoBGmain(:,:,s);
    RmonoBG=P.RmonoBGmain(:,:,s);
    if D.bMain==1
        LbinoBG=P.LbinoBGmain(:,:,s);
        RbinoBG=P.RbinoBGmain(:,:,s);
        LbinoFG=P.LbinoFGmain(:,:,s);
        RbinoFG=P.RbinoFGmain(:,:,s);
    else
        LbinoBG=P.LbinoBG(:,:,s);
        RbinoBG=P.RbinoBG(:,:,s);
        LbinoFG=P.LbinoFG(:,:,s);
        RbinoFG=P.RbinoFG(:,:,s);
    end

    %MAKE L & R STIM TEXTURE FROM RMS CONTRAST IMAGES

    %MONO BACKGROUND TEXTURE
    if strcmp(C.stmMskTexMBG,'coloredNoise')
        mskNoise=coloredNoise(P.PszXY,-1);
        CIsz = 99.9./100;
        [Qlohi]=quantile(mskNoise(:),[(1 - CIsz)./2  1-(1 - CIsz)./2]);
        maskTexMBGL = (mskNoise - Qlohi(1))./(Qlohi(2) - Qlohi(1));
        maskTexMBGR = maskTexMBGL; % XXX NEED TO ADD DISPARITY HERE
    elseif strcmp(C.stmMskTexMBG,'gray')
        maskTexMBGL=ones(size(stmLfull))*D.gry;
        maskTexMBGR=maskTexMBGL;
    elseif strcmp(C.stmMskTexMBG,'black')
        maskTexMBGL=zeros(size(stmLfull));
        maskTexMBGR=maskTexMBGL;
    elseif strcmp(C.stmMskTexMBG,'white')
        maskTexMBGL=ones(size(stmLfull));
        maskTexMBGR=maskTexMBGL;
    elseif strcmp(C.stmMskTexMBG,'grayUp')
        maskTexMBGL=ones(size(stmLfull)).*(D.gry*(1+D.gryMult));
        maskTexMBGR=maskTexMBGL;
    elseif strcmp(C.stmMskTexMBG,'grayDn')
        maskTexMBGL=ones(size(stmLfull)).*(D.gry*(1-D.gryMult));
        maskTexMBGR=maskTexMBGL;
    end

    %BINO BACKGROUND TEXTURE
    if strcmp(C.stmMskTexBBG,'coloredNoise')
        mskNoise=coloredNoise(P.PszXY,-1);
        CIsz = 99.9./100;
        [Qlohi]=quantile(mskNoise(:),[(1 - CIsz)./2  1-(1 - CIsz)./2]);
        maskTexBBGL = (mskNoise - Qlohi(1))./(Qlohi(2) - Qlohi(1));
        maskTexBBGR = maskTexBBGL; % XXX NEED TO ADD DISPARITY HERE
    elseif strcmp(C.stmMskTexBBG,'gray')
        maskTexBBGL=ones(size(stmLfull))*D.gry;
        maskTexBBGR=maskTexBBGL;
    elseif strcmp(C.stmMskTexBBG,'black')
        maskTexBBGL=zeros(size(stmLfull));
        maskTexBBGR=maskTexBBGL;
    elseif strcmp(C.stmMskTexBBG,'white')
        maskTexBBGL=ones(size(stmLfull));
        maskTexBBGR=maskTexBBGL;
    elseif strcmp(C.stmMskTexBBG,'grayUp')
        maskTexBBGL=ones(size(stmLfull)).*(D.gry*(1+D.gryMult));
        maskTexBBGR=maskTexBBGL;
    elseif strcmp(C.stmMskTexBBG,'grayDn')
        maskTexBBGL=ones(size(stmLfull)).*(D.gry*(1-D.gryMult));
        maskTexBBGR=maskTexBBGL;
    end



    %FOREGROUND TEXTURE
    if strcmp(C.stmMskTexFG,'coloredNoise')
        mskNoise=coloredNoise(P.PszXY,-1);
        CIsz = 99.9./100;
        [Qlohi]=quantile(mskNoise(:),[(1 - CIsz)./2  1-(1 - CIsz)./2]);
        maskTexFGL = (mskNoise - Qlohi(1))./(Qlohi(2) - Qlohi(1));
        maskTexFGR = maskTexFGL; % XXX NEED TO ADD DISPARITY HERE
    elseif strcmp(C.stmMskTexFG,'gray')
        maskTexFGL=ones(size(P.LccdRMS(:,:,s)))*D.gry;
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'black')
        maskTexFGL=zeros(size(P.LccdRMS(:,:,s)));
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'white')
        maskTexFGL=ones(size(P.LccdRMS(:,:,s)));
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'grayUp')
        maskTexFGL=ones(size(stmLfull)).*(D.gry*(1+D.gryMult));
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'grayDn')
        maskTexFGL=ones(size(stmLfull)).*(D.gry*(1-D.gryMult));
        maskTexFGR=maskTexFGL;
    end



    %FOREGROUND TEXTURE
    if strcmp(C.stmMskTexFG,'coloredNoise')
        mskNoise=coloredNoise(P.PszXY,-1);
        CIsz = 99.9./100;
        [Qlohi]=quantile(mskNoise(:),[(1 - CIsz)./2  1-(1 - CIsz)./2]);
        maskTexFGL = (mskNoise - Qlohi(1))./(Qlohi(2) - Qlohi(1));
        maskTexFGR = maskTexFGL; % XXX NEED TO ADD DISPARITY HERE
    elseif strcmp(C.stmMskTexFG,'gray')
        maskTexFGL=ones(size(P.LccdRMS(:,:,s)))*D.gry;
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'black')
        maskTexFGL=zeros(size(P.LccdRMS(:,:,s)));
        maskTexFGR=maskTexFGL;
    elseif strcmp(C.stmMskTexFG,'white')
        maskTexFGL=ones(size(P.LccdRMS(:,:,s)));
        maskTexFGR=maskTexFGL;
    end

    %COMBINE FG AND BACKGROUND
    % FG M B
    maskTexL=(maskTexFGL.*LbinoFG) + (maskTexMBGL.*LmonoBG) + (maskTexBBGL.*(~LmonoBG& ~LbinoFG));
    maskTexR=(maskTexFGR.*RbinoFG) + (maskTexMBGR.*RmonoBG) + (maskTexBBGR.*(~RmonoBG & ~RbinoFG));

    %NO IMAGE
    if D.stmMskMode==0
        stmL=zeros(size(stmLfull))+maskTexL;
        stmR=zeros(size(stmRfull))+maskTexR;
    %FULL IMAGE
    elseif D.stmMskMode==1
        stmL=P.LccdRMS(:,:,s);
        stmR=P.RccdRMS(:,:,s);
    %FOREGROUND ONLY
    elseif D.stmMskMode==2
        stmL=P.LccdRMS(:,:,s).*LbinoFG+(~LbinoFG.*maskTexL);
        stmR=P.RccdRMS(:,:,s).*RbinoFG+(~RbinoFG.*maskTexR);
    %FULL BACKGROUND ONLY
    elseif D.stmMskMode==3
        stmL=P.LccdRMS(:,:,s).*LbinoBG+(~LbinoBG.*maskTexL);
        stmR=P.RccdRMS(:,:,s).*RbinoBG+(~RbinoBG.*maskTexR);
    %MONO BACKGROUND ONLY
    elseif D.stmMskMode==4
        if strcmp(P.LorRall(s),'L')
            stmL=P.LccdRMS(:,:,s).*LmonoBG+(~LmonoBG.*maskTexL);
            stmR=maskTexR;
            %stmR=P.RccdRMS(:,:,s);
        elseif strcmp(P.LorRall(s),'R')
            stmL=maskTexL;
            stmR=P.RccdRMS(:,:,s).*RmonoBG+(~RmonoBG.*maskTexR);
            first=find(P.imgNumAll==P.imgNumAll(s),1,'first');
            %stmL=P.LccdRMS(:,:,s);
        end
    %BINO BACKGROUND + FOREGROUND
    elseif D.stmMskMode==5
        stmL=P.LccdRMS(:,:,s).*(LbinoBG+LbinoFG)+(~(LbinoBG+LbinoFG).*maskTexL);
        stmR=P.RccdRMS(:,:,s).*(RbinoBG+RbinoFG)+(~(RbinoBG+RbinoFG).*maskTexR);
    %MONO + FOREGROUND
    elseif D.stmMskMode==6
        if strcmp(P.LorRall(s),'L')
            % UNCOMMENT FOR MULTIPLE MONO BACKGROUND
            %stmL=P.LccdRMS(:,:,s).*(LmonoBG+LbinoFG)+(~(LmonoBG+LbinoFG).*maskTexL);
            %stmR=P.RccdRMS(:,:,s).*(RbinoFG)+(~(RbinoFG).*maskTexR);
            stmL= P.LccdRMS(:,:,s).*(LmonoBG+LbinoFG) + (~LmonoBG & ~LbinoFG).*maskTexL;
            stmR= P.RccdRMS(:,:,s).*RbinoFG+ (~RbinoFG).*maskTexR;
                            %stmR=P.RccdRMS(:,:,s);
        elseif strcmp(P.LorRall(s),'R')
            %stmL=P.LccdRMS(:,:,s).*(LbinoFG)+(~(LbinoFG).*maskTexL);
            %stmR=P.RccdRMS(:,:,s).*(RmonoBG+RbinoFG)+(~(RmonoBG+RbinoFG).*maskTexR);
            stmL= P.LccdRMS(:,:,s).*LbinoFG + (~LbinoFG).*maskTexL;
            stmR= P.RccdRMS(:,:,s).*(RmonoBG+RbinoFG) + (~RmonoBG & ~RbinoFG).*maskTexR;
        end
    elseif D.stmMskMode==7
        maskTex2=ones(size(P.LccdRMS(:,:,s)))*D.gry;
        if strcmp(P.LorRall(s),'L')
            stmL=maskTexL.*LmonoBG+(~LmonoBG.*maskTex2);
            stmR=maskTexR;
            %stmR=P.RccdRMS(:,:,s);
        elseif strcmp(P.LorRall(s),'R')
            stmL=maskTexL;
            stmR=maskTexR.*RmonoBG+(~RmonoBG.*maskTex2);
            first=find(P.imgNumAll==P.imgNumAll(s),1,'first');
            %stmL=P.LccdRMS(:,:,s);
        end
    end
    stmTex{1} = Screen('MakeTexture', D.wdwPtr, stmL,[],[],2);
    stmTex{2} = Screen('MakeTexture', D.wdwPtr, stmR,[],[],2);

    % ----------------------------------------------------------------------

    %%RING TEXTURE
    if strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'arrow')
        prbRingTex{1} = Screen('MakeTexture', D.wdwPtr, prb,[],[],2);
        prbRingTex{2} = Screen('MakeTexture', D.wdwPtr, prb,[],[],2);
    end

    % ----------------------------------------------------------------------

    %GET RESPONSE PRB CUTOUT
    if strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'arrow')

        %% RESPONSE PROBE
        if D.bShowRingMask
            stmLprb=stmLfull;
            stmRprb=stmRfull;
        else
            stmLprb=stmL;
            stmRprb=stmR;
        end
        try
            stmLR=cropImageCtrInterp(stmLprb,     prbCrpLoc(1,:),D.pixPerDegXY.*D.prbXYdeg+rmpSz,'linear',0);
        catch
            stmLR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz))).*D.gry;
        end
        try
            stmRR=cropImageCtrInterp(stmRprb,     prbCrpLoc(2,:),D.pixPerDegXY.*D.prbXYdeg+rmpSz,'linear',0);
        catch
            stmRR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz))).*D.gry;
        end

        %CIRCULAR WINDOW -> ALPHA C
        sz=max(size(stmLR))-rmpSz;
        szs=[size(stmLR,1) size(stmLR,2)];
        %XXX
        W=cosWindowFlattop(szs,sz-5,rmpSz+5);

        prbR=cell(2,1);
        prbR{1}(:,:,1)=stmLR;
        prbR{1}(:,:,2)=stmLR;
        prbR{1}(:,:,3)=stmLR;
        prbR{1}(:,:,4)=W;

        prbR{2}(:,:,1)=stmRR;
        prbR{2}(:,:,2)=stmRR;
        prbR{2}(:,:,3)=stmRR;
        prbR{2}(:,:,4)=W;

        prbTexR{1} = Screen('MakeTexture', D.wdwPtr, prbR{1},[],[],2);
        prbTexR{2} = Screen('MakeTexture', D.wdwPtr, prbR{2},[],[],2);


    elseif ~isempty(prbR)
    prbTexR{1} = Screen('MakeTexture', D.wdwPtr, prbR,[],[],2);
    prbTexR{2} = Screen('MakeTexture', D.wdwPtr, prbR,[],[],2);
    else
    prbTexR{1} = Screen('MakeTexture', D.wdwPtr, prb,[],[],2);
    prbTexR{2} = Screen('MakeTexture', D.wdwPtr, prb,[],[],2);
    end

    % ----------------------------------------------------------------------
    %BORDER PROBE
    if strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'arrow')
        stmLR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz)));
        stmRR=ones(round(fliplr(D.pixPerDegXY.*D.prbXYdeg+rmpSz)));
        switch D.prbBtexType
        case 'wht'
            donothing;
        case 'blk'
            stmLR=stmLR*0;
            stmRR=stmRR*0;
        case 'gry'
            stmLR=stmLR*D.gry-.2;
            stmRR=stmRR*D.gry-.2;
        end
        prbR{1}(:,:,1)=stmLR;
        prbR{1}(:,:,2)=stmLR;
        prbR{1}(:,:,3)=stmLR;
        prbR{1}(:,:,4)=W;

        prbR{2}(:,:,1)=stmRR;
        prbR{2}(:,:,2)=stmRR;
        prbR{2}(:,:,3)=stmRR;
        prbR{2}(:,:,4)=W;

        prbTexB{1} = Screen('MakeTexture', D.wdwPtr, prbR{1},[],[],2);
        prbTexB{2} = Screen('MakeTexture', D.wdwPtr, prbR{2},[],[],2);
    else
        prbTexB{1} = Screen('MakeTexture', D.wdwPtr, prb,[],[],1);
        prbTexB{2} = Screen('MakeTexture', D.wdwPtr, prb,[],[],1);
    end

    % ----------------------------------------------------------------------
    %%IMAGE PROBE
    if strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'arrow')
        sz=[size(prb,2) size(prb,1)];

        loc=prbCrpLoc(1,:);
        bFG = loc(2) > P.AitpRCfgnd(s,2)+D.prbRadius(1); %XXX 1?
        try
            if P.LorRall(s)=='L' && bFG
                bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
            elseif P.LorRall(s)=='L'
                bgnL=cropImageCtrInterp(double(P.LmonoBG(:,:,s) | LbinoFG), loc,sz,'linear',0);

            elseif P.LorRall(s)=='R' && bFG
                bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
            elseif P.LorRall(s)=='R'
                bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
            end
        catch
            bgnL=zeros(round(fliplr(size(prb(:,:,1)))));
        end

        loc=prbCrpLoc(2,:);
        bFG = loc(2) < P.AitpRCfgnd(s,2)+D.prbRadius(1);
        try
            if P.LorRall(s)=='L'  && bFG
                bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
            elseif P.LorRall(s)=='L'
                bgnR=zeros(round(fliplr(size(prb(:,:,1)))));

            elseif P.LorRall(s)=='R' && bFG
                bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
            elseif P.LorRall(s)=='R'
                bgnR=cropImageCtrInterp(double(P.RmonoBG(:,:,s) | RbinoFG), loc,sz,'linear',0);
            end
        catch
            bgnR=zeros(round(fliplr(size(prb(:,:,1)))));
        end
        %%%%%%%%%%%%%

        %HIDE RING (BY ALPHA) IF OCCLUDED
        prbIL=prb(:,:,4);
        prbIR=prb(:,:,4);
        if D.partialOcclude
            prbIL(logical(ceil(bgnR)))=0;
            prbIR(logical(ceil(bgnL)))=0;
        end

        prbI{1}(:,:,1)=prb(:,:,1);
        prbI{1}(:,:,2)=prb(:,:,2);
        prbI{1}(:,:,3)=prb(:,:,3);
        prbI{1}(:,:,4)=prbIL;

        prbI{2}(:,:,1)=prb(:,:,1);
        prbI{2}(:,:,2)=prb(:,:,2);
        prbI{2}(:,:,3)=prb(:,:,3);
        prbI{2}(:,:,4)=prbIR;

        prbTex{1} = Screen('MakeTexture', D.wdwPtr, prbI{1},   [],[],1);
        prbTex{2} = Screen('MakeTexture', D.wdwPtr, prbI{2},   [],[],1);

    else
        prbTex{1} = Screen('MakeTexture', D.wdwPtr, prb,   [],[],1);
        prbTex{2} = Screen('MakeTexture', D.wdwPtr, prb,   [],[],1);
    end

    % LINE PROBES
    if isvar('prbL')
        prbTexL{1} = Screen('MakeTexture', D.wdwPtr, prbL,   [],[],1);
        prbTexL{2} = Screen('MakeTexture', D.wdwPtr, prbL,   [],[],1);
    else
        prbTexL=[];
    end
end

function[]=depthProbeDrawStim(D,stmTex)
    stmSqr=D.plySqrPix;
    if D.secondary==1
%Bottom
        stmSqr(2)=D.plySqrPix(2)+D.secSep*D.pixPerDegXY(2);
        stmSqr(4)=D.plySqrPix(4)+D.secSep*D.pixPerDegXY(2);
        Screen('DrawTexture', D.wdwPtr, stmTex, [], stmSqr);

 %ADJUST STIM IF MULTIPLE
 %Top
        stmSqr(2)=D.plySqrPix(2)-D.secSep*D.pixPerDegXY(2);
        stmSqr(4)=D.plySqrPix(4)-D.secSep*D.pixPerDegXY(2);
        Screen('FillRect', D.wdwPtr, D.gry, stmSqr);
    else
        Screen('DrawTexture', D.wdwPtr, stmTex, [], stmSqr);
    end
end

function [] = depthProbeDrawInfo(t,s,D,C,P,R,bInfo,depth,depthC,depthTrue,depthTrueC,vrgArcMinC,vrgArcMinTrueC,vrsArcMin,elvArcMinTrue,prbCurXYpix,Ind);

    fFlags={'-','A','B','C'};
    Top=50+D.secSep/2*D.pixPerDegXY;
    Bottom=Top*-1-D.secSep/2*D.pixPerDegXY;
    Left=-200;
    Right=20;
    Line=20;
    fg=D.wht;
    bg=D.gry;

    if bInfo==1

        Screen('TextSize',D.wdwPtr,C.textSizeInfo);
        if R.Xhat(t)==0
            Response='-';
        else
            Response=num2str(R.Xhat(t));
        end

        if strcmp(D.tstMode,'')
            D.tstModeStr='-';
        else
            D.tstModeStr=D.tstMode;
        end

        %BOTTOM LEFT
        if exist('vrgArcMinC','var')
            rect3=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*2];
            text3=['verg.      ' sprintf('%+06.2f', vrgArcMinC)];
            Screen('DrawText',D.wdwPtr,text3,rect3(1),rect3(2),fg,bg); %DISPARITY OF CURRENT PROBE IN NON-TEST
        end
        rect2=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*1];
        rect1=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*0];
        text2=['verg. true ' sprintf('%+06.2f',vrgArcMinTrueC)];
        text1=['Response  ' Response];
        Screen('DrawText',D.wdwPtr,text2,rect2(1),rect2(2),fg,bg); %TRUE DISPARITY OF BACKGROUND
        Screen('DrawText',D.wdwPtr,text1,rect1(1),rect1(2),fg,bg); %RESPONSE

        rect7=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-1];
        text7=['vrsArcMin  ' sprintf('%+06.2f',vrsArcMin)];
        Screen('DrawText',D.wdwPtr,text7,rect7(1),rect7(2),fg,bg); %DEPTH

        rect6=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-2];
        text6=['elvArcMin  ' sprintf('%+06.2f',elvArcMinTrue)];
        Screen('DrawText',D.wdwPtr,text6,rect6(1),rect6(2),fg,bg); %DEPTH

        if D.bCyc==1;
            if exist('depthC','var')
                rect5=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-3];
                text5=['Dist. C    ' sprintf('%+06.3f',depthC)];
                Screen('DrawText',D.wdwPtr,text5,rect5(1),rect5(2),fg,bg); %DEPTH
            end
            if exist('depthTrueC','var')
                rect4=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-4];
                text4=['Dist.TC    ' sprintf('%+06.3f',depthTrueC)];
                Screen('DrawText',D.wdwPtr,text4,rect4(1),rect4(2),fg,bg); %DEPTH
            end
        elseif D.bCyc==1;
            if exist('depth','var')
                rect5=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-3];
                text5=['Dist.      ' sprintf('%+06.3f',depth)];
                Screen('DrawText',D.wdwPtr,text5,rect5(1),rect5(2),fg,bg); %DEPTH
            end
            if exist('depthTrue','var')
                rect4=[D.scrnCtr(1)+Left,C.fixStm{1}(2)-Bottom-Line*-4];
                text4=['Dist.T     ' sprintf('%+06.3f',depthTrue)];
                Screen('DrawText',D.wdwPtr,text4,rect4(1),rect4(2),fg,bg); %DEPTH
            end
        end


        %TOP RIGHT
        if isfield(P,'rmsMonoBG')
            rms_bFG_bBG=P.rms_bFG_bBG(t);
            rms_bFG_mBG=P.rms_bFG_mBG(t);
            rms_bBG_mBG=P.rms_bBG_mBG(t);
        else
            rms_bFG_bBG='-';
            rms_bFG_mBG='-';
            rms_bBG_mBG='-';
        end

        if isfield(P,'dcMonoBG')
            dc_bFG_bBG=P.dc_bFG_bBG(t);
            dc_bFG_mBG=P.dc_bFG_mBG(t);
            dc_bBG_mBG=P.dc_bBG_mBG(t);
        else
            dc_bFG_bBG='-';
            dc_bFG_mBG='-';
            dc_bBG_mBG='-';
        end
        if isfield(D,'subjInd')
            subj=D.Subjs{D.subjInd};
        else
            subj='-';
        end

        rect4=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line*-5];
        rect3=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line*-4];
        rect2=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line*-3];
        rect1=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line*-2];
        rect0=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line*-1];
        rect00=[D.scrnCtr(1)+Right,C.fixStm{1}(4)-Top+Line* 0];
        text4='             RMS      DC';
        text3=sprintf('bFG-bBG | %+06.3f | %+06.3f |',rms_bFG_bBG,dc_bFG_bBG);
        text2=sprintf('bFG-mBG | %+06.3f | %+06.3f |',rms_bFG_mBG,dc_bFG_mBG);
        text1=sprintf('bBG-mBG | %+06.3f | %+06.3f |',rms_bBG_mBG,dc_bBG_mBG);
        text0=sprintf('RMS %+06.3f | DC %+06.3f',D.RMS,D.DC);
        text00=sprintf('RMSmask %+06.3f  %+06.3f', D.RMSmaskAdj,D.RMSmaskAdj*D.RMS);
        Screen('DrawText',D.wdwPtr,text4,rect4(1),rect4(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text3,rect2(1),rect3(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text2,rect2(1),rect2(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text1,rect1(1),rect1(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text0,rect0(1),rect0(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text00,rect00(1),rect00(2),fg,bg);

        %TOP LEFT
        rect6=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*5];
        rect5=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*4];
        rect4=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*3];
        rect3=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*2];
        rect2=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*1];
        rect1=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*0];
        text6=['sample    ' num2str(s)];
        text5=['img       ' num2str(P.imgNumAll(s))];
        text4=['RprbTrace ' D.prbTrace];
        text3=['DvnWidth  ' num2str(P.width(s))];
        text2=['PrbLoc    ' D.prbLocType];
        text1=['Flag      ' fFlags{R.flag(t)}];
        Screen('DrawText',D.wdwPtr,text6,rect1(1),rect6(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text5,rect1(1),rect5(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text4,rect1(1),rect4(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text3,rect1(1),rect3(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text2,rect1(1),rect2(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text1,rect1(1),rect1(2),fg,bg);

        %BOTTOM Right
        rect4=[D.scrnCtr(1)+Right,C.fixStm{1}(2)-Bottom-Line*2];
        rect3=[D.scrnCtr(1)+Right,C.fixStm{1}(2)-Bottom-Line*1];
        rect2=[D.scrnCtr(1)+Right,C.fixStm{1}(2)-Bottom-Line*0];
        rect1=[D.scrnCtr(1)+Right,C.fixStm{1}(2)-Bottom-Line*-1];

        text4=['Subj          ' subj];
        text3=sprintf('Prb PosXY    [ %+06.1f, %+06.1f]',prbCurXYpix(1),prbCurXYpix(2)) ;
        text2=sprintf('StimXYarcMin [ %+06.1f, %+06.1f]',D.stmXYdeg(1)*60,  D.stmXYdeg(2)*60) ;
        text1=['TestMode    ' D.tstModeStr];

        Screen('DrawText',D.wdwPtr,text4,rect4(1),rect4(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text3,rect3(1),rect3(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text2,rect2(1),rect2(2),fg,bg);
        Screen('DrawText',D.wdwPtr,text1,rect1(1),rect1(2),fg,bg);
    elseif D.bShowFlags==1
        Screen('TextSize',D.wdwPtr,C.textSizeInfo);
        text1=['Flag      ' fFlags{R.flag(t)}];
        rect1=[D.scrnCtr(1)+Left, C.fixStm{1}(4)-Top-Line*0];
        Screen('DrawText',D.wdwPtr,text1,rect1(1),rect1(2),fg,bg);
    end
end

function [] = depthProbeDrawProbes(s,j,D,P,ItpXYT,ItpXY,ItpXYB,prbTex,prbTexR,prbTexB,prbTexL,prbRingTex,bDVNzone)
    prbSqrT=zeros(1,4);
    prbSqrL=zeros(1,4);
    prbSqr =zeros(1,4);
    prbSqrBorder=zeros(1,4);

    if P.LorRall(s)=='L'
        A=1;
    elseif P.LorRall(s)=='R'
        A=2;
    end

    if  D.bUsePrb || D.secondary

        %% DRAW IMAGE PROBE
        %ADJUST PROBE IF MULTIPLE STIM
        if D.secondary==1
            for i = [1,3]
                prbSqrT(i)=ItpXYT(1)+D.prbPlySqrPix(i)-D.prbRadius;
                prbSqrL(i)=ItpXYT(1)+D.prbLPlySqrPix(i)-D.prbRadius;
            end
            for i = [2,4]
                prbSqrT(i)=ItpXYT(2)+D.prbPlySqrPix(i)-D.prbRadius+D.secSep*D.pixPerDegXY(2);
                prbSqrL(i)=ItpXYT(2)+D.prbLPlySqrPix(i)-D.prbLradiusY+D.secSep*D.pixPerDegXY(2);
            end
        end
        if D.bUsePrb==1 & ( A==j | ~D.bHideNanchor )
            if  strcmp(D.prbType,'gaussRing') && D.bShowRingMask
                Screen('DrawTexture', D.wdwPtr, prbTexR{j},    [], prbSqrT); % DRAW RING
                %Screen('DrawTexture', D.wdwPtr, prbTexL{j},    [], prbSqrL); % DRAW LINE PROBE - FOR TESTING
            end
            if strcmp(D.prbType,'arrow')
                Screen('DrawTexture', D.wdwPtr, prbTexL{j},    [], prbSqrL); % DRAW LINE PROBE
            else
                Screen('DrawTexture', D.wdwPtr, prbTex{j},    [], prbSqrT); % DRAW TEXTURE
            end
        end

        %% DRAW RESPONSE PROBE
        if D.secondary==1
            for i = [1,3]
                prbSqr(i)=ItpXY(1)+D.prbPlySqrPix(i)-D.prbRadius;
            end
            for i = [2,4]
                prbSqr(i)=ItpXY(2)+D.prbPlySqrPix(i)-D.prbRadius-D.secSep*D.pixPerDegXY(2);
            end

            if isempty(prbTexR)
                Screen('DrawTexture', D.wdwPtr, prbTex{j},     [], prbSqr);
            else
                if bDVNzone
                    if P.LorRall(s)=='L'
                        k=1;
                    elseif P.LorRall(s)=='R'
                        k=2;
                    end
                    Screen('DrawTexture', D.wdwPtr, prbTexR{k},    [], prbSqr);
                    if D.bRspRing==1 && ~isempty(prbRingTex) && ~isempty(prbRingTex{k})
                        Screen('DrawTexture', D.wdwPtr, prbRingTex{k}, [], prbSqr);
                    end
                else
                    Screen('DrawTexture', D.wdwPtr, prbTexR{j},    [], prbSqr);
                    if D.bRspRing==1 && ~isempty(prbRingTex) && ~isempty(prbRingTex{j})
                        Screen('DrawTexture', D.wdwPtr, prbRingTex{j}, [], prbSqr);
                    end
                end
            end
        elseif strcmp(D.D.tstMode,'')
            if isempty(prbTexR)
                Screen('DrawTexture', D.wdwPtr, prbTex,     [], prbSqr);
            else
                if bDVNzone
                    if P.LorRall(s)=='L'
                        k=1;
                    elseif P.LorRall(s)=='R'
                        k=2;
                    end
                    Screen('DrawTexture', D.wdwPtr, prbTexR{k},    [], prbSqr);
                else
                    Screen('DrawTexture', D.wdwPtr, prbTexR{j},    [], prbSqr);
                end
            end
        end

        %% DRAW BORDER PROBE
        if D.secondary==1 && D.bPrbB
            for i = [1,3]
                prbSqrBorder(i)=ItpXYB(1)+D.prbPlySqrPix(i)-D.prbRadius;
                if A == 1
                    %prbSqrBorder(i)=prbSqrBorder(i)+D.prbRadius;
                    prbSqrBorder(i)=prbSqrBorder(i);
                elseif A == 2
                    %prbSqrBorder(i)=prbSqrBorder(i)-D.prbRadius;
                    prbSqrBorder(i)=prbSqrBorder(i);
                end
            end
            for i = [2,4]
                prbSqrBorder(i)=ItpXYB(2)+D.prbPlySqrPix(i)-D.prbRadius+D.secSep*D.pixPerDegXY(2);
            end

            if ~strcmp(D.prbBtexType,'none') && (strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'arrow'))
                Screen('DrawTexture', D.wdwPtr, prbTexB{A},    [], prbSqrBorder);
                Screen('DrawTexture', D.wdwPtr, prbTex{A},    [], prbSqrBorder);
            end
            %elseif strcmp(D.D.tstMode,'')
            %    Screen('DrawTexture', D.wdwPtr, prbTex,    [], prbSqrB);
        end
    end
end

function [prb,prbR,prbL,Lratio]=depthProbeGen(D,G)
    prbR=[];
    prbInt=1;
    Lratio=[];
    if strcmp(D.prbType,'gabor')
        D.prbXYdeg=D.prbXYdegG;

        G.szPix            = 128;
        G.frqCpd
        [x, y]= meshgrid(smpPos(G.szPix,G.szPix));
        Prb = gabor2D(x,y,0,0,G.frqCpd,G.thetaDeg,G.phaseDeg,G.sigmaXdeg,G.sigmaYdeg,1,0);
        G.DC=.1;
        G.RMS=.1;
        PRB = (Prb+min(min(Prb))*2)/2;

        prb=zeros(size(Prb,1),size(Prb,2),4);
        prb(:,:,1)=PRB;
        prb(:,:,2)=PRB;
        prb(:,:,3)=PRB;
        prb(:,:,4)=Prb*255;
    elseif strcmp(D.prbType,'gaussian')
        Prb=gaussKernel2D([],20,0);
        Prb=Prb/max(max(Prb));
        PRB=Prb/(1-D.gry)+D.gry;

        prb=zeros(size(Prb,1),size(Prb,2),4);
        prb(:,:,1)=PRB*prbInt;
        prb(:,:,2)=PRB*prbInt;
        prb(:,:,3)=PRB*prbInt;
        prb(:,:,4)=Prb*255;
        Lratio=size(Prb,1)/size(Prb,2);
    elseif strcmp(D.prbType,'gaussRing') || strcmp(D.prbType,'gaussRing2') || strcmp(D.prbType,'arrow')
        x=linspace(-10,10,21);
        [x,y]=meshgrid(x);
        z = sin(sqrt(.03*x.^2 + .03*y.^2))/3;
        mult=1/max(max(z));
        z=z*mult;
        z(z<.98)=0;
        z=z.^100;

        Prb=D.prbFace*ones(size(z));
        prb(:,:,1)=Prb;
        prb(:,:,2)=Prb;
        prb(:,:,3)=Prb;
        prb(:,:,4)=z;

    elseif strcmp(D.prbType,'circle')
        %CREATE LOGICAL CIRCLE IN MATR.X
        N = 101;
        Ra = floor(sqrt(N.^2/pi/2)); %"... circle to fill half the area of the matrix..."
        ii = abs(floor((1:N) - N/2));
        prb = double(hypot(ii',ii) <= Ra);         % MATLAB R2016b and later

        %GET RGB CHANNELS
        Prb=prb;
        Prb(prb == 1)=D.prbFace*255;
        Prb(prb == 0)=D.prbBack*255;

        %GET ALPHA CHANNEL
        Alp=prb;
        Alp(prb ~= 0)=D.prbAlpha*255;
        Alp(prb == 0)=0;

        % MAKE RGB IMAGE
        prb=zeros(size(prb,1),size(prb,2),4);
        prb(:,:,1)=Prb;
        prb(:,:,2)=Prb;
        prb(:,:,3)=Prb;
        prb(:,:,4)=Alp;
    end
    % --------------------------------------------------
    %RESPONSE PROBE TAKES REGULAR CIRCLE
    if strcmp(D.prbType,'gaussRing2')
    N = 21;
    Ra = floor(sqrt(N.^2/pi/2)); %"... circle to fill half the area of the matrix..."
    ii = abs(floor((1:N) - N/2));
    Prb = double(hypot(ii',ii) <= Ra);         % MATLAB R2016b and later
    prbtmp=Prb;
    Prb(prbtmp == 1)=D.prbFace*255;
    Prb(prbtmp == 0)=D.prbBack*255;

                                    %GET ALPHA CHANNEL
    Alp=prbtmp;
    Alp(prbtmp ~= 0)=D.prbAlpha*255;
    Alp(prbtmp == 0)=0;

    prbR=zeros(size(prbtmp,1),size(prbtmp,2),4);
    prbR(:,:,1)=Prb;
    prbR(:,:,2)=Prb;
    prbR(:,:,3)=Prb;
    prbR(:,:,4)=Alp;
    end

    if strcmp(D.prbType,'arrow') || strcmp(D.prbType,'gaussRing')
        W=10;
        H=W*2;
        Prb=zeros(round(H/3),W);
        Prb2=zeros(1,W);
        Prb3=zeros(round(H/2.5),W);
        if mod(W,2)==0
            Prb(:,W/2:W/2+1)=1;
        else
            Prb(:,floor(W/2):ceil(W/2))=1;
            Prb2(:,floor(W/2):ceil(W/2))=1;
        end
        Prb=[Prb;Prb2;Prb3;Prb2;Prb];
        %images(Prb)
        Lratio=size(Prb,1)/size(Prb,2);

        Alp=Prb;
        Alp(Prb ~= 0)=1;
        Alp(Prb == 0)=0;
        prbL=zeros(size(Prb,1),size(Prb,2),4);
        prbL(:,:,1)=Prb;
        prbL(:,:,2)=Prb;
        prbL(:,:,3)=Prb;
        prbL(:,:,4)=Alp*.8;
    else
        prbL=[];
    end
end


function [D,C]=depthProbeBgnd(t,D,C,P,startflag)

    %MASK START CONDITIONS
    C  = psyDemoCreateCH(D,C);
    if startflag==1
        D.bUsePrb=1;
        %CREATE MASK ON mskReset
        if (t ==1 || mod(t-1,D.nTrlMskReset)==0) && D.bUseMsk
            %CLOSE OLD MASK
            if exist('tex1oF','var')
                Screen('Close', tex1oF);
            end
        end
        [~,C.tex1oF]=psyDemoCreateMask(D,C,1,1);
    end

    if D.secondary==1
        %Noise Mask
        D=psyDemoPresentMask(D,C,[],   D.secSep,0,1,1,1);
        %Gray Masks
        D=psyDemoPresentMask(D,C,[],   D.secSep,1,0,1,1);
        D=psyDemoPresentMask(D,C,[],-1*D.secSep,1,0,1,1);

        %COUNTDOWN
        psyDemoCountDown(t,D,C,P.PszXY,startflag);
        D=psyDemoPresentMask(D,C,[],   D.secSep,1,0,1,1);
        D=psyDemoPresentMask(D,C,[],-1*D.secSep,1,0,1,1);

        %middle crosshairs
        hair=C.hairThick.*D.pixPerDegXY(1);
        rect=[D.scrnCtr(1),D.scrnCtr(2)+D.secSep*D.pixPerDegXY(2),D.scrnCtr(1),D.scrnCtr(2)-D.secSep*D.pixPerDegXY(2)];
        for z = 0:D.bStereo
            Screen('SelectStereoDrawBuffer', D.wdwPtr, z);
            Screen('DrawLine', D.wdwPtr, D.wht, rect(1),rect(2),rect(3),rect(4),hair); %bino fixation cross
        end
        %Cross
        D=psyDemoPresentMask(D,C,[],   D.secSep,1,1,0,1,1);
        D=psyDemoPresentMask(D,C,[],-1*D.secSep,1,1,0,1,2);
        %Fill
        D=psyDemoPresentMask(D,C,[],   D.secSep,1,1,1,0);
        D=psyDemoPresentMask(D,C,[],-1*D.secSep,1,1,1,0);
    else
        %Noise Mask
        D=psyDemoPresentMask(D,C,[],   D.secSep,0,1,1,1);
        %Gray Masks
        D=psyDemoPresentMask(D,C,[],   D.secSep,1,0,1,1);
        D=psyDemoPresentMask(D,C,[],-1*D.secSep,1,0,1,1);

        %COUNTDOWN
        psyDemoCountDown(t,D,C,P.PszXY,startflag);
        D=psyDemoPresentMask(D,C);
    end
end
