clearvars -except P
if ~isvar('P')
  loadP
end
for s = 1:length(P.LorRall)
    LorR=P.LorRall(s);
    if LorR=='L'
        mono=P.LmonoBGmain(:,:,s);
    elseif LorR=='R'
        continue
        mono=P.RmonoBGmain(:,:,s);
    end
    sums=sum(mono,2);
    row=find(sums==max(sums),1);

    col1=find(mono(row,:),1);
    col2=find(fliplr(mono(row,:)),1);

    startMain=col1-1;
    endMain=col2+1;

    startAll=sum(cumprod(~mono,2),2)+1;
    [sr,~]=find(startAll==min(startAll),1);
    sc=min(startAll);

    endAll=P.PszXY(1)-sum(cumprod(fliplr(~mono),2),2)+1;
    %endAll=sum(cumprod(~mono,2,'reverse'),2)+1;
    [er,~]=find(endAll==min(endAll),1);
    ec=min(endAll);

    START=endAll-(endMain-startMain);

    remain=P.PszXY(1)-endAll;
    img=zeros(size(mono));


    for i = 1:length(START)
        try
        img(i,1:START(i))=P.LccdRMS(i,1:START(i),s);
        catch
            img(i,1:START(i))
        end
        img(i,START(i)+1:START(i)+1+remain(i))=P.LccdRMS(i,endAll(i):P.PszXY(1),s);
    end
    figure(1)
    imagesc([P.LccdRMS(:,:,s) P.RccdRMS(:,:,s)].^.4);
    formatImage;
    z=caxis;

    figure(2)
    imagesc(img.^.4)
    caxis(z);
    formatImage;

    drawnow;
    waitforbuttonpress;


end
