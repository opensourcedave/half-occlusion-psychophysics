function [D,prbCurXYpix,prbCurXYpixB,prbCurXYpixBorder]=depthProbeCurPix(P,D,s,startflag)
if ~isvar('startflag')
    startflag=1;
end
if startflag==1
% INIT PROBE
    D = depthProbeAdjustPrbInit(P,D,s);
else
% DEG 2 PIXELS
    D.prbXYoffsetPix= D.prbXYoffsetDeg.*D.pixPerDegXY;
end

%% CURENT LOCATION OF IMAGE PROBES
prbCurXYpix        = P.PszXY./2-D.prbXYoffsetPix;

%% CURENT LOCATION OF RESPOSE PROBE
prbCurXYpixB       = P.PszXY./2-D.prbXYoffsetPixB;

%% CURENT LOCATION OF BORDER PROBE
prbCurXYpixBorder  = P.PszXY./2-D.prbXYoffsetPixBorder;
