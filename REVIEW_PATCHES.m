sub=[1 1 1];
quad=0;
% --------------------

if ~isvar('obj')
    loadObj;
end
if ~isvar('P')
    P=obj.loadP;
end

[D,C,G,tmp]=obj.loadD(sub);
unpackOpts(tmp);

dspArcMin=dspArcMinAll(dspArcMinInd,:);
minMaxPixDVN=minMaxPixDVNall(minMaxPixDVNind,:);
indTrl=1:size(P.LphtCrpAll,3);

inds=obj.select_inds(sub);
Ind=select_quad_ind(obj,inds,quad);

ListenChar(2);
[R,P,D,exitflag] = ExpDVNadjust(D,C,P,G,indTrl,0,1,Ind);
