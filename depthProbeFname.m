function[sfname,lfname]=depthProbeFname(D,P,S,pass,subjName,rndSd);
if exist('P','var')
    rndSd=P.rndSd;
end

serv='/Volumes/Data/Project_PsyData/HOC/NAT/NAT/PILOT/'
loc ='/Users/Shared/Data/Project_Psydata/HOC/NAT/NAT/PILOT/';
userv=[serv subjName filesep];
uloc=[loc  subjName filesep];
serv=strrep(serv,'/',filesep);
loc=strrep(loc,'/',filesep);

fname=['HOC_NAT_NAT__' D.hostName '_'  D.prbLocType '-' num2str(D.stmMskMode)  num2str(D.bPrbB) num2str(D.prbHideOnMove) '_Rnd' num2str(S.rndSdTO.Seed) '-' num2str(rndSd) '_' subjName '_PILOT_' num2str(pass) ];

sfname=[serv fname];
lfname=[loc  fname];
