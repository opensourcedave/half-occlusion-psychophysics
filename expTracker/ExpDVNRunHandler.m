function [S,D,exitflag] = ExpDVNRunHandler(S,subjName,rawBlksIndTrl,bUseFeedback,bSKIPSYNCTEST,bDEBUG,sizeMult,timeMult,defFname,blksIndTrl)
%USE TO INTEGRATE RUN TRACKER WITH depthProbeAdjust

run(defFname);

tbAddToPath('/Applications/Psychtoolbox');

if timeMult ~= 1
    warning('This timeMult is invalid in this experiment')
end
if bUseFeedback == 1
    warning('Feedback has not been implemented yet')
end

D.sizeMult=sizeMult;
D.stmXYdeg=sizeMult*D.stmXYdeg;
S.subjName=subjName;

%THERE MAY NOT BE ENOUGH STIM FOR A FULL BLOCK-> HANDLE THIS
if isvar('blksIndTrl')
    S.blksIndTrl=S.blksIndTrl;
else
    S.blksIndTrl=S.indTrl(rawBlksIndTrl);
end
S.blksIndTrl(S.blksIndTrl>numel(S.P.LorRall))=[];

bParent=1;
bTst=0;
[R,P,D,exitflag] = ExpDVNadjust(D,C,S.P,G,S.blksIndTrl,bParent,bTst);

S=rmfield(S,'P');

%UPDATE D & P
S.D=D;
S.C=C;
S.G=G;
try
    %MERGE R INTO Sexp
    f = fieldnames(R);
    for i = 1:length(f)
        S.(f{i}) = R.(f{i});
    end
catch ME
    S.R=R;
end
