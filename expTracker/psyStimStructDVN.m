function [S] = psyStimStructDVN(P,trlPerLvl,~,~,rndSd,bRandomize,rndSdTO, S)
%     rndSd     :   Randomization seed to use
%     bRandomize:   whether or not to randomize stimulus order
%     rndSdTO   :   random seed to shuffle trial order
if ~isvar('S')
    S=struct();
end

P=structSingle2Double(P);
S.trlPerLvl = trlPerLvl;                         % number of trials per stimulus level


%SHUFFLE TRIAL ORDER BASED UPON rndSdTO IF PROVIDED
if bRandomize == 1
    if rndSdTO ~= 0
        S.rndSdTO=rng(rndSdTO);
    end
    indTrl       = randsample(S.trlPerLvl,S.trlPerLvl);    % SHUFFLE TRIAL ORDER
else
    indTrl=(1:length(P.LorRall))';
end
S.indTrl=indTrl;
S.P=P;

%%SHUFFLE ALL FIELDS OF RELEVANT SIZE ACCORDING TO INDTRL
%if bRandomize == 1
%    fields=fieldnames(P);
%    valSz=size(P.LorRall,1);
%    for i = 1:length(fields)
%        sz=size(P.(fields{i}));
%        if iscell(P.(fields{i}))
%            continue
%        end
%        if     numel(sz)==4 && sz(4)==valSz && sz(3)~=valSz && sz(2)~=valSz && sz(1)~=valSz
%            S.(fields{i})=P.(fields{i})(:,:,:,indTrl);
%        elseif numel(sz)==3 && sz(3)==valSz && sz(2)~=valSz && sz(1)~=valSz
%            S.(fields{i})=P.(fields{i})(:,:,indTrl);
%        elseif numel(sz)==2 && sz(1)==valSz && sz(2)~=valSz
%            S.(fields{i})=P.(fields{i})(indTrl,:);
%        elseif any(sz==valSz)
%            error(['Bad field size: ' fields{i}]);
%        end
%    end
%end
