%no cues
fname='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-2_0_jburge-wheatstone_DNW_001-0-0.mat';
%no mono
fname='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_2-1_0_jburge-wheatstone_DNW_001-0-0.mat'
%no bbg
fname='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_2-2_0_jburge-wheatstone_DNW_001-0-0.mat'

fname='/Volumes/Data/Project_PsyData/DVN/NAT/NAT/DNW/DVN_NAT_NAT_Adj_1-1_0_jburge-wheatstone_DNW_001-0-0.mat'

S=load(fname);

if ~isfield(S,'Xsort')
    [~,indSort]    =sort(S.X);
    S.Xsort        =S.X(indSort);
    S.XhatSort     =S.Xhat(indSort);
    S.XstarSort    =S.Xstar(indSort);
end

depthProbeAdjustPlotResults(S)
