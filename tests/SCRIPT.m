trlPerLevel=500;
rndSd=1;
rndSdTO=1;
bRandomize=1;

saveDTB='/Volumes/Data/Project_Databases/LRSI/Patch/DVN';
bTracker=0

fileParams_HOC_NAT_NAT2D_est
%NAT_NAT2D_est

dspArcMin=dspArcMinAll(dspArcMinInd,:);
minMaxPixDVN=minMaxPixDVNall(minMaxPixDVNind,:);
fname = daVinciPatchFname(saveDTB,dspArcMin,minMaxPixDVN,fgndORbgnd,ctrORedg,PszXY,rndSd);
HOC=load(fname);
S=psyStimStructHalfOcc(HOC,trlPerLevel,0,0,rndSd,bRandomize,rndSdTO);