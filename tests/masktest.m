close all

exponent=-1;
PszXY=[150,150];
nBack=30;
x=normrnd(0,1,PszXY(1),PszXY(2),nBack);
max(max(abs(x(:,:,end))))

P=.2;
m=rand(PszXY(1),PszXY(2));
k=100;

X=repmat(1:PszXY(1),PszXY(2),1);
Y=repmat([1:PszXY(2)]',1,PszXY(1));

t=0;
v=0;
dt=.0016;
while true


    prbTrns=rand(PszXY(1),PszXY(2));
    lind=prbTrns>.2;
    prbDire=randi(8,PszXY(1),PszXY(2));

    a=-k./m.*x(:,:,end);
    v=v+dt*a;
    x(:,:,1)=[];
    x(:,:,end+1)=x(:,:,end)+dt*v;

    %imagesc(x(:,:,end))
    %caxis([-2 2])
    %colormap hot
    %plot(1:nBack,squeeze(x(1,1,:)),'.')
    %ylim([-2 2])

    image=x(:,:,end);
    % 2D FFT, AMP AND PHASE SPECTRUM
    Ifft   = fftshift(fft2(ifftshift(image)));
    Iphs   = angle(Ifft);
    Iamp   = abs(Ifft);


    % X,Y FREQUENCY COORDINATES (i.e. U,V)
    [U,V] = meshgrid( smpFrq(PszXY(1),PszXY(1)), smpFrq(PszXY(1),PszXY(2)));
    % RADIAL DISTANCE
    R     = sqrt(U.^2 + V.^2);
    % FILTER TO INTRODUCE PINKISH-NOISE
    F         = R.^exponent;
    indCtrX   = PszXY(1)/2+1;
    indCtrY   = PszXY(2)/2+1;
    F(indCtrY,indCtrX) = 1;

    I = F.*Iamp.*[cos(Iphs) + 1i.*sin(Iphs)];

    % Inverse 2D-FFT
    I = real(fftshift(ifft2(ifftshift(I))));

    imagesc(I)
    colorbar
    colormap gray
    caxis([-.15 .15])
    drawnow
end
