function [Xstar,depth] = depthProbeGeometricOld(pdCppZm,LorR,width,depthImgDisp,Xmap,PszXY,X,Y,LdvnCrp,RdvnCrp)

%% INIT
%HANDLE OPTIONAL VARIABLES
if ~exist('X','var') && ~exist('Y','var')
    [X,Y]=meshgrid(1:PszXY(1),1:PszXY(2));
elseif ~exist('X','var')
    [X,~]=meshgrid(1:PszXY(1),1:PszXY(2));
elseif ~exist('Y','var')
    [~,Y]=meshgrid(1:PszXY(1),1:PszXY(2));
end

%GET EYE COORDINATES
I=LRSIcameraIPD(1);
LE=[-I/2,0,0];
RE=[ I/2,0,0];
CppXYZ=[0, 0, pdCppZm];

%% GET XZ METERS COORDINATES OF OUTER DVN REGION
%GET XY PIXEL COORDINATES OF HALF OCCLUSION EDGE (INCLUSIVE)
yPix=ceil(PszXY(2)/2);
if strcmp(LorR,'L')
    row=logical(LdvnCrp(yPix,:)); %MIDDLE ROW OF PATCH
    xPix=find(row==1,1,'last');
elseif strcmp(LorR,'R')
    row=logical(RdvnCrp(yPix,:)); %MIDDLE ROW OF PATCH
    xPix=find(row==1,1,'first');
end

%INTERPOLATE TO GET X & Z COORDINATES
xM=interp2(X,Y,fliplr(Xmap),        xPix,yPix);
zM=interp2(X,Y,fliplr(depthImgDisp), xPix,yPix);

b=[xM, 0, zM];

%% GET XZ METERS COORDINATES OF QUERY PROBE
if strcmp(LorR,'L')
    XPix=[xPix+width,0,yPix];
elseif strcmp(LorR,'R')
    XPix=[xPix-width,0,yPix];
end

%INTERPOLATE TO GET X & Z COORDINATES
xM=interp2(X,Y,(Xmap),         xPix,yPix);
zM=interp2(X,Y,(depthImgDisp), xPix,yPix);

c=[xM, 0, zM];

%% FIND INTERSECTION OF LINES RE-B & LE-C
if strcmp(LorR,'L')
    CitpXYZ=intersectLinesFromPoints(RE,b,LE,c);
elseif strcmp(LorR,'R')
    CitpXYZ=intersectLinesFromPoints(RE,c,LE,b);
end

Xstar=CitpXYZ(3);

%[IvrgArcMinC]   = 60*vergenceFromRangeXYZVec('C',I,CitpXYZ); %vergence at target
%[CppVrgArcMinC] = 60*vergenceFromRangeXYZVec('C',I,CppXYZ); %vergence at focus (screen)
%Xstar           = IvrgArcMinC-CppVrgArcMinC; %DISPARITY MAP: disparity = theta_target - theta_focus
