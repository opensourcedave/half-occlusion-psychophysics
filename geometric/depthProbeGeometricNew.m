function [depthGeom,vrgArcMinGeom] = depthProbeGeometricNew(depth,widthm,LocFromLorRm,fgndZm,IPDm)
% LorR         - which anchor
% depth        - distance from observer to query point
% LocFromLorRm - width in meters from edge (left edge if anchor L) of half occlusion to query point
% widthm       - width of half occlusion zone in meters
% fgndZm       - cartesian distance from observer to foreground

%distance from background to geometric
Z=fgndZm*depth*LocFromLorRm/(fgndZm*LocFromLorRm - fgndZm*widthm + depth*IPDm);
depthGeom=depth-Z;

A=IPDm/2;
vrgArcMinGeom=60*rad2deg(pi/2 + atan((A*(depth + (fgndZm*(LocFromLorRm - widthm))/A))/(fgndZm*depth)) + atan(fgndZm/(A - IPDm)));
