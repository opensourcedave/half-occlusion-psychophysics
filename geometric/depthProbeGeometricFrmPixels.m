function[Xstar] = depthProbeGeometricFrmPixels(LorR,depth,widthPix,LocFromLorRPix,fgndZm,IPdm,D)
[X,Y]=meshgrid(1:P.PszXY(1),1:P.PszXY(2));

%GET WIDTH OF HALF OCCLUSION ZONE AS IT APPEARS IN THE PROJECTION PLANE
widthPP=interp1(X,D.pdCppXm,width)

%GET DISTANCE FROM FOREGROUND TO BACKGROUND
fgnd2bgndZm=depth-fgdnZm;

%USE TRIANGLE CONGRUENCY TO GET WIDTH OF MONOCULAR DAVINCI ZONE AS IT APPEARS IN THE BGND
widthm=fgdn2bgndZm*widthPP/fgndZm
%XXX width + 1, width - 1?

LocFromLorRm=LocFromLorRPix*widthm/widthPix;

[Xstar] = depthProbeGeometricNew(LorR,depth,widthm,LocFromLorRm,fgndZ,IPDm)