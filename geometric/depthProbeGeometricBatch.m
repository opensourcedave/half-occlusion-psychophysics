function [Xstar,XstarFit,vrgArcMinGeom] = depthProbeGeometricBatch(P,D,indTrl)
Xstar=zeros(length(indTrl),1);
XstarFit=zeros(length(indTrl),1);
vrgArcMinGeom=zeros(length(indTrl),1);
[X,Y]=meshgrid(1:P.PszXY(1),1:P.PszXY(2));

[dspArcMinDisplay]=dspLRSI2dspDisplayNaive(P.dspArcMinAll,P.PszXY,D.stmXYdeg,'jburge-wheatstone');

ctr=P.PszXY./2;
if ~isfield(D,'prbPlySqrPix')
    D.prbPlySqrPix=[958.0000  538.0000  962.9221  542.8922];
end

if ~isfield(P,'depthImgDisp')
    P.depthImgDisp      = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
    bCreate=1;
else
    bCreate=0;
end

% XXX
bCreate=1;
method='new';

if ~isfield(D,'pCppXm') || ~isfield(D,'pCppYm') || ~isfield(D,'pCppZm')
    [D.pdCppXm,D.pdCppYm,D.pdCppZm] = psyProjPlaneAnchorEye('C',1,1,P.PszXY,D.stmXYdeg,D.hostName);
    P.Xmap              = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
    P.Ymap              = zeros(P.PszXY(2),P.PszXY(1),length(P.LorRall));
end

% XXX
x1=1.0146; %Foreground edge depth
y1=x1;
m=.5320;   %fitted slope
b=y1-m*x1;
linfunc=@(x) m*x + b;
X=P.depthTrueC;
XstarFit=linfunc(X);

for t = 1:length(indTrl)
    s=indTrl(t);
    if bCreate==1
        [~,~,tmp1,~,~,tmp3,tmp4]= disparityMapDisplay( P.AitpRC(:,:,s),P.ABitpRC(:,:,s),P.BitpRC(:,:,s),P.LorRall(s), D.stmXYdeg,P.PszXY,D.pdCppXm,D.pdCppYm,D.pdCppZm);
        P.depthImgDisp(:,:,s)=tmp1;
        P.Xmap(:,:,s)=tmp3;
        P.Ymap(:,:,s)=tmp4;
    end

    % XXX
    if P.LorRall(s)=='R'
        P.width(s)=P.width(s);
    end

    if strcmp(method,'new')
        switch D.prbLocType
          case 'm1'
            D.prbLocDsp=0;
          case 'm2'
            D.prbLocDsp=P.width(s)/4;
          case 'm3'
            D.prbLocDsp=P.width(s)/2;
          case 'm4'
            D.prbLocDsp=P.width(s)/4*3;
          case 'm5'
            D.prbLocDsp=P.width(s);
          case 'be'
            D.prbLocDsp         = -1*D.prbRadius;
          case 'bc'
            if strcmp(P.LorRall(s),'L')
              D.prbLocDsp       = -1*(P.beginWidth(s)/2);
            elseif strcmp(P.LorRall(s),'R')
              D.prbLocDsp       = -1*(P.endWidth(s)/2);
            end
        end

        % XXX
        %if P.LorRall(s)=='L'
        %    D.prbLocDsp=floor(D.prbLocDsp);
        %elseif P.LorRall(s)=='R'
        %    D.prbLocDsp=ceil(D.prbLocDsp);
        %end

        fgndZm = dsp2depthNaive(dspArcMinDisplay,.065,1,1);
        fgndZm=1+(-1*fgndZm);
        %fgndZm       = interp2(X,Y,P.depthImgDisp(:,:,s),ctr(1),ctr(2))
        ewidthm=D.prbLocDsp*D.multFactorXY(1)/D.pixPerMxy(1);
        widthm=P.width(s)*D.multFactorXY(1)/D.pixPerMxy(1);

        [Xstar(t), vrgArcMinGeom(t)] = depthProbeGeometricNew(P.depthTrueC(t),widthm,ewidthm,fgndZm,.065);
    elseif strcmp(method,'old')
        Xstar(t) = depthProbeGeometricOld(D.pdCppZm,P.LorRall(s),P.width(s),P.depthImgDisp(:,:,s),P.Xmap(:,:,s),P.PszXY,X,Y,P.LdvnCrpAll(:,:,s),P.RdvnCrpAll(:,:,s));
    end

end
