function [t,V,exitflag]=depthProbePatchViewer(P,D,t,s,Ind,prbCurXYpix,bPosition,wallORcross,bEdit,V,bInit)
if ~isvar('bInit')
    bInit=1;
end
if ~isvar('V')
    V=struct();
end
if ~isvar('bEdit')
    bEdit=1;
end
if ~isvar('wallORcross')
    wallORcross='wall';
end
bMove=1;


% TABLE
if isvar('Ind')
    t=find(Ind.ind==t);

    mXhat=mean(Ind.Xhat(t,:),2);
    bias=(Ind.Xhat(t,:)-Ind.X(t,:));
    sstd=sqrt((Ind.Xhat(t,:)-mXhat).^2);

    flag=[Ind.flag(t,:)-1 any(Ind.flag(t,:)-1,2)]';
    Xhat=[Ind.Xhat(t,:) mXhat]';
    Xm  =[bias Ind.Xm(t)]';
    Ym  =[sstd Ind.Y(t)]';

    clc
    if size(Ind.Subjs',1) < size(flag,1)
        Ind.Subjs{end+1}='All';
    end
    TB=table(Ind.Subjs',flag,Xhat,Ind.X(t,1)-Xhat,Xm,Ym,'VariableNames',{'Subject','flag','Xhat','error','bias','std'});
    disp(TB)
end

% IMAGES
figure(669)
ax=subPlot([3 1],1,1);
title('Depth')
imagesc(P.depthImgDisp(:,:,s))
colorbar
formatImage;
colormap(ax,flipud(gray));
formatFigure('','',['Anchor ' P.LorRall(s) ', Ind ' num2str(s) ', Image ' num2str(P.imgNumAll(s))])
if bInit
    %p=[ 0 0 576 740];
    %op=[661 972];
    if islinux
        p(1:2)=[0,0];
    else
        p(1:2)=[1682,0];
    end
    p(3:4)=[762 1130];
    if bPosition
        set(gcf,'Position',p)
    end
end

subPlot([3 1],2,1)
if strcmp(wallORcross,'wall')
    imagesc(real([P.LccdRMS(:,:,s) P.RccdRMS(:,:,s)].^.4))
elseif strcmp(wallORcross,'cross')
    imagesc(real([P.RccdRMS(:,:,s) P.LccdRMS(:,:,s)].^.4))
end
formatImage;
formatFigure('','',num2str(prbCurXYpix));

ax=subPlot([3 1],3,1);
L=-1*P.LbinoFGmain(:,:,s)+(~P.LbinoFGmain(:,:,s) & ~P.LmonoBGmain(:,:,s));
R=-1*P.RbinoFGmain(:,:,s)+(~P.RbinoFGmain(:,:,s) & ~P.RmonoBGmain(:,:,s));
imagesc([L R]);
formatImage;
formatFigure('','','Blue=FG');
colormap(ax,cmapBWR);

% ABOVE
figure(670)
if isvar('Ind')
    Subjs=Ind.Subjs;
    Xhat=Ind.Xhat(t,:);
else
    Subjs='NUL';
    Xhat=nan;
end

[t,V,exitflag]=depthProbeAbove3(s,t,P,D,Subjs,Xhat,prbCurXYpix,bEdit,V);
set(gca,'PlotBoxAspectRatio',[3 3 1])
if bInit==1
    p=[p(1)+p(3)+1 0 762 1130];
    %p=[ p(3)+1 0 576 740];
    %op=[661 972];
    if bPosition
        set(gcf,'Position',p)
    end
end
