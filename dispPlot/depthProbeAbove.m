function []=depthProbeAbove(Subjs,P,D,Xhat,s,t,prbCurXYpix)


r=ceil(P.PszXY(2)/2);
c=ceil(P.PszXY(1)/2);
[X,Y]  =meshgrid(1:P.PszXY(1),1:P.PszXY(2));

%GET DEPTH OF QUERY POINT
%prbCurXYpix        = -1*S.XhatLocXYpix(t,:);

if strcmp(P.LorRall(s),'L')
  depthTrue       = interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1),prbCurXYpix(2));
elseif strcmp(P.LorRall(s),'R')
  depthTrue       = interp2(X,Y,fliplr(P.depthImgDisp(:,:,s)),     prbCurXYpix(1)-3-P.width(s),prbCurXYpix(2));
end

%GET COORDINATES OF QUERY POINT
[LitpXYT,RitpXYT,prbTxyzM] = psyCorrespondingPointsFromDepth(depthTrue,prbCurXYpix,P.LorRall(s),P.PszXY,D.multFactorXY,D.pixPerMxy,D.scrnCtr,D.LExyz,D.RExyz,D.PPxyz);
%prbTxyzM(1)=prbTxyzM(1)*-1

%GET FGND & BCKGRND
img=P.depthImgDisp(:,:,s);
if P.LorRall(s)=='R'
    mm=minmax(find(fliplr(P.RmonoBGmain(r,:,s))))+4;
elseif P.LorRall(s)=='L'
    mm=minmax(find(P.LmonoBGmain(r,:,s)));
end
m=repmat(ceil(P.PszXY(1)/2),1,2);

%GET x coordinates in meters
distXYm=D.CppZm.*tand(D.stmXYdeg./2);
x=linspace(-distXYm(1),distXYm(1),P.PszXY(1));
dist=abs(x(2)-x(1));

%GET FOREGROUND EDGE
if P.LorRall(s)=='L'
    FG=P.LbinoFGmain(:,:,s);
elseif P.LorRall(s)=='R'
    FG=P.RbinoFGmain(:,:,s);
end
dn=r-5;
up=r+5;
men=nanmean(img,1);
med=nanmedian(img,1);
mid  =P.depthImgDisp(r,:,s);
midup=P.depthImgDisp(up,:,s);
middn=P.depthImgDisp(dn,:,s);
midFG  =logical(FG(r, :));
midFGup=logical(FG(up,:));
midFGdn=logical(FG(dn,:));
[EdgInd  ,EdgIndAll]  =edg_fun(s,x,P,mid,c);
[EdgIndUp,EdgIndAllUp]=edg_fun(s,x,P,midup,c);
[EdgIndDn,EdgIndAllDn]=edg_fun(s,x,P,middn,c);

Ledg=intersectLinesFromPoints(D.LExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 1.25],[D.RExyz(1) 0 1.25]);
Redg=intersectLinesFromPoints(D.RExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 1.25],[D.RExyz(1) 0 1.25]);


%GET ALL EDGES

%XXX
if P.LorRall(s)=='L'
  distInd=c-EdgInd(1);
elseif P.LorRall(s)=='R'
  EdgInd(1)=EdgInd(1)-1;
  distInd=c-EdgInd(1);
end
mult=dist*(distInd);
x=x+mult;
Ledg=intersectLinesFromPoints(D.LExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 depthTrue],[D.RExyz(1) 0 depthTrue]);
Redg=intersectLinesFromPoints(D.RExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 depthTrue],[D.RExyz(1) 0 depthTrue]);

shift=abs((Ledg-Redg)/2);
if P.LorRall(s)=='R'
    shift=shift*-1;
end


%GEOMETRIC DEPTH
if P.LorRall(s)=='L'
  depthG=intersectLinesFromPoints(D.LExyz,[0 0 depthTrue],D.RExyz,Redg);
elseif P.LorRall(s)=='R'
  depthG=intersectLinesFromPoints(D.RExyz,[0 0 depthTrue],D.LExyz,Ledg);
end

%Xhat
depthxyz=zeros(size(Xhat,2),3);
for i = 1:size(Xhat,2)
    depth=Xhat(t,i);
    if P.LorRall(s)=='L'
    depthxyz(i,:)=intersectLinesFromPoints(D.LExyz,depthG,[-0.2 0 depth],[0.2 0 depth]);
    elseif P.LorRall(s)=='R'
    depthxyz(i,:)=intersectLinesFromPoints(D.RExyz,depthG,[-0.2 0 depth],[0.2 0 depth]);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%5
%PLOT BACKGROUND
plot([0 0]); hold on
plot_bg_fun(midup,EdgIndAllUp,x,'r',midFGup,shift);
plot_bg_fun(middn,EdgIndAllDn,x,'b',midFGdn,shift);
plot_bg_fun(mid,  EdgIndAll,  x,'k',midFG,shift);

%MEAN AND MEDIAN
%var(A,0,dim)
%plot(x,men,'r');
%plot(x,med,'b');

%MONO ZONE
plot([D.LExyz(1), Ledg(1)],[D.LExyz(3), Ledg(3)],'k','LineWidth',.75)
plot([D.RExyz(1), Redg(1)],[D.RExyz(3), Redg(3)],'k','LineWidth',.75)

%GEOMETRIC
for i = 1:size(depthxyz,1)
    if P.LorRall(s)=='L'
        if depthTrue>depth
            plot([D.LExyz(1), 0],[0, depthTrue],'k--')
        else
            plot([D.LExyz(1), depthxyz(i,1)],[0, depthxyz(i,3)],'k--')
        end
    elseif P.LorRall(s)=='R'
        if depthTrue>depth
            plot([D.RExyz(1), 0],[0, depthTrue],'k--')
        else
            plot([D.RExyz(1), depthxyz(i,1)],[0, depthxyz(i,3)],'k--')
        end
    end
end

%Center line
%plot([0 0],[0 1.25],'k--')

%GEOMETRIC RESPONSE
scatter(depthG(1),depthG(3),120,'s','MarkerFaceColor','k','MarkerEdgeColor','none','LineWidth',2);
%SUBJECT RESPONSE
for i = 1:size(depthxyz,1)
    [~,color]=def_Exp_subj(Subjs{i});
    scatter(depthxyz(i,1),depthxyz(i,3), 75,['o' color],'MarkerFaceColor','w','MarkerEdgeColor',[0 0 0],'LineWidth',2); %anchor
end
%scatter(0,depth, 75,'o','MarkerFaceColor','w','MarkerEdgeColor',[0 0 0],'LineWidth',2); %cartesian
%TRUE RESPONSE
scatter(0,prbTxyzM(3),75,'o','MarkerFaceColor','k','MarkerEdgeColor','k','LineWidth',2);


ylim([.95 1.21])
xlim([-.02, .02])
%titl=[P.LorRall(s) ' ' num2str(s) ': k - middle, r - mean, b - median'];
titl=[P.LorRall(s) ' ' num2str(s)];
formatFigure('X-position (meters)','Distance (meters)',titl);
hold off

% -------------------------------
% FUNCTIONS
end
function [EdgInd, EdgIndAll]=edg_fun(s,x,P,mid,c)
    if     P.LorRall(s)=='L'
        edgInd=find(diff(mid)==min(diff(mid(c-15:c+15))));
        edgeInd=edgInd(1);
        EdgInd=edgInd+1;

        edgIndAll=find(abs(diff(mid))>.01);
        EdgIndAll=[1 edgIndAll+1 length(x)];
    elseif P.LorRall(s)=='R'
        edgInd=find(diff(mid)==max(diff(mid(c-40:c+00))));
        edgeInd=edgInd(end);
        EdgInd=edgInd+1;

        edgIndAll=find(abs(diff(mid))>.01);
        EdgIndAll=[1 edgIndAll+1 length(x)];
    end

end

function plot_bg_fun(Mid,EdgIndAll,X,color,FG,shift)
    for i = 1:length(EdgIndAll)-1
        rng=EdgIndAll(i):(EdgIndAll(i+1)-1);
        l=zeros(size(X));
        l(rng)=1;

        x=X(~FG & l)
        mid=Mid(~FG & l);
        %plot(x(rng),mid(rng),color,'LineWidth',3); hold on
        plot(x,mid,color,'LineWidth',3); hold on

        x=X(FG & l);
        mid=Mid(FG & l);
        %plot(x(rng),mid(rng),color,'LineWidth',3); hold on
        plot(x,mid,color,'LineWidth',3); hold on
    end
end
