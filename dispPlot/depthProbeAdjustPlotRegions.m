function [] = depthProbeAdjustPlotRegions(P,s,prbCurXYpix,D,thickness)
if ~isvar('Boffset')
    Boffset=0;
end
DC=.7;
fac=1.1;
rmpSz=6;
%MS=10;

allI=find(P.imgNumAll==P.imgNumAll(s));
num=find(allI==s);


pts=distribute(1:P.PszXY(2),1:P.PszXY(1));

r=D/2-1.5;
MS=D;

prbCurXYpixTMP(1)=prbCurXYpix(1)+1;
prbCurXYpixTMP(2)=prbCurXYpix(2)-1;
prbCrpInd = logicalCircle(P.PszXY,prbCurXYpixTMP,r);
if P.LorRall(s)=='L'
    stm=P.LccdRMS(:,:,s);
elseif P.LorRall(s)=='R'
    stm=P.RccdRMS(:,:,s);
end
prbCrp=(prbCrpInd.*stm).^fac;

fh=figure(374);
I1=[P.LbinoFG(:,:,s) P.RbinoFG(:,:,s)];
I2=[P.LbinoBG(:,:,s) P.RbinoBG(:,:,s)];
I3=[P.LmonoBG(:,:,s) P.RmonoBG(:,:,s)];
I1=logicalOutline(I1,1);
I2=logicalOutline(I2,1);
I3=logicalOutline(I3,1);
I=(I1 | I2 | I3);
%I=logicalSurround(I,thickness,2);

imagesc(~I); hold on
%plotObj=scatterRel(prbCurXYpix(1),prbCurXYpix(2),MS,'o','MarkerFaceColor','none','MarkerEdgeColor','w');
formatImage

%imagesc(P.LccdRMS(:,:,s).^fac); hold on
%imagesc(DC*ones(size(P.RccdRMS(:,:,s))))
%imagesc(prbCrp+(DC*~prbCrpInd))
%imagesc(DC*ones(size(P.RccdRMS(:,:,s))))
%imagesc(P.RccdRMS(:,:,s).^fac .* P.RbinoFG(:,:,s) + (~P.RbinoFG(:,:,s) & ~prbCrpInd)*DC  + prbCrp); hold on
%imagesc(P.LccdRMS(:,:,s).^fac .* P.LbinoFG(:,:,s) + ~P.LbinoFG(:,:,s)*DC); hold on
%imagesc(P.LccdRMS(:,:,s).^fac .* P.LbinoFG(:,:,s) + (~P.LbinoFG(:,:,s) & ~prbCrpInd)*DC + prbCrp); hold on
%imagesc(P.RccdRMS(:,:,s).^fac .* ~P.RbinoBG(:,:,s) + P.RbinoBG(:,:,s)*DC); hold on
%imagesc(P.LccdRMS(:,:,s).^fac .* ~P.LbinoBG(:,:,s) + P.LbinoBG(:,:,s)*DC); hold on
%imagesc(P.RccdRMS(:,:,s).^fac .* ~P.RmonoBG(:,:,s) + (P.RmonoBG(:,:,s))*DC); hold on
%imagesc(P.RccdRMS(:,:,s).^fac);hold on
%imagesc(P.LccdRMS(:,:,s).^fac .* ~P.LmonoBG(:,:,s) + (P.LmonoBG(:,:,s) & ~prbCrpInd)*DC + prbCrp); hold on
%imagesc(P.LccdRMS(:,:,s).^fac .* ~P.LmonoBG(:,:,s) + (P.LmonoBG(:,:,s))*DC); hold on
%imagesc(P.RccdRMS(:,:,s).^fac .* ~P.RmonoBG(:,:,s) + (P.RmonoBG(:,:,s) & ~prbCrpInd)*DC + prbCrp); hold on
%imagesc(prbCrp+(DC*~prbCrpInd))
%%%%%%%%%%%%
%% PHT IMAGE
