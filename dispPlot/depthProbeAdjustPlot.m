function [] = depthProbeAdjustPlot(P,s,prbCurXYpix,D,Boffset)
if ~isvar('Boffset')
    Boffset=0;
end
if isfield(P,'depthImgDisp')
    II=1;
else
    II=0;
end
fac=.30;
nfac=1.0;
bRMSimg=0;
cax=[0 1.1];
bflush=1;

b1oF=1;
fBorder=[90 90];
rBorder=[60 60];
%MS=10;

%mini=min(min(P.dspArcMinImgDisp(:,:,s)));
%maxi=max(max(P.dspArcMinImgDisp(:,:,s)));
allI=find(P.imgNumAll==P.imgNumAll(s));
num=find(allI==s);

r=D/2-1.5;
MS=D;

prbCurXYpixTMP(1)=prbCurXYpix(1)+1;
prbCurXYpixTMP(2)=prbCurXYpix(2)-1;
prbCrpInd = logicalCircle(P.PszXY,prbCurXYpixTMP,r);

if bRMSimg
    stmL=P.LccdRMS(:,:,s);
    stmR=P.RccdRMS(:,:,s);
else
    m=max(max(abs([P.LphtCrpAll(:,:,s) P.RphtCrpAll(:,:,s)])));
    stmL=(P.LphtCrpAll(:,:,s)/m).^fac;
    stmR=(P.RphtCrpAll(:,:,s)/m).^fac;
    DC=mean(mean(stmL));
    DC=.55;
end

sz=size(stmL);
if b1oF
    if bflush
        XY=[1+P.PszXY(1)+2*fBorder(2) 1+P.PszXY(2)+1.5*fBorder(1)];
        gray=ones(sz(1)+1.5*rBorder(1), sz(2)+1.5*rBorder(2));
        szg=size(gray);
        diff=fBorder-rBorder
        diff(2)=round(XY(1)/2-szg(2)/2)-1
        gray2=borderFnc(szg,diff,gray,1,'T');
        gray=borderFnc(szg,diff,gray,1,'B');
    else
        XY=[1+P.PszXY(1)+2*fBorder(2) 1+P.PszXY(2)+2*fBorder(1)];
        gray=ones(sz(1)+2*rBorder(1), sz(2)+2*rBorder(2));
        szg=size(gray);
        gray=borderFnc(sz,fBorder-rBorder,gray,1);
        gray2=borderFnc(sz,fBorder-rBorder,gray,1);
    end
    XY(2)=XY(2)*2;
    [~,nsMsk]=psyMask1overFrect(XY,0,[0 0],0);
    nsMsk2 =nsMsk(1:XY(2)/2,:);
    nsMsk=nsMsk((XY(2)/2+1:end),:);



    if bflush
        flog2=borderFnc(sz,fBorder,[],1,'ET');
        flog=borderFnc(sz,fBorder,[],1,'EB');

        size(gray)
        size(nsMsk2)
        nsMsk=nsMsk.*(~flog & ~gray);
        nsMsk2=nsMsk2.*(~flog2 & ~gray2);

        stmL2     =borderFnc(sz,fBorder,stmL,     1,'ET')+(nsMsk2.^nfac)*DC;
        stmR2     =borderFnc(sz,fBorder,stmR,     1,'ET')+(nsMsk2.^nfac)*DC;
        rspProbeInd=borderFnc(sz,fBorder,prbCrpInd,1,'ET');

        stmL     =borderFnc(sz,fBorder,stmL,     1,'EB')+(nsMsk.^nfac)*DC;
        stmR     =borderFnc(sz,fBorder,stmR,     1,'EB')+(nsMsk.^nfac)*DC;
        prbCrpInd=borderFnc(sz,fBorder,prbCrpInd,1,'EB');
    else
        flog=borderFnc(sz,fBorder,[],1);

        nsMsk=nsMsk.*(~flog & ~gray);
        nsMsk2=nsMsk2.*(~flog2 & ~gray);

        stmL2     =borderFnc(sz,fBorder,stmL,     1)+(nsMsk2.^nfac)*DC;
        stmR2     =borderFnc(sz,fBorder,stmR,     1)+(nsMsk2.^nfac)*DC;
        stmL     =borderFnc(sz,fBorder,stmL,     1)+(nsMsk.^nfac)*DC;
        stmR     =borderFnc(sz,fBorder,stmR,     1)+(nsMsk.^nfac)*DC;
        prbCrpInd=borderFnc(sz,fBorder,prbCrpInd,1);
        rspProbeInd=borderFnc(sz,fBorder,prbCrpInd,1);
    end
else
    nsMsk=[];
    flog=ones(sz);
    fBorder=[0 0];
end

if P.LorRall(s)=='L'
    stm=stmL;
    stm2=stmL2;
elseif P.LorRall(s)=='R'
    stm=stmR;
    stm2=stmR2;
end
prbCrp=(prbCrpInd.*stm);
rspProbe=(rspProbeInd.*stm2);

xshift=round(P.PszXY(1)/2-prbCurXYpix(1));
yshift=0;
prbCur(1)=prbCurXYpix(1)+xshift;
prbCur(2)=prbCurXYpix(2);
rspProbeInd=logicalShift(rspProbeInd,yshift,xshift);
rspProbe=logicalShift(rspProbe,yshift,xshift,0);

%%%%%%%%%%%%
%% PHT IMAGE
fh=figure(666);
dims=[6,2];
if bflush
    flush='EB';
    flush2='ET';
else
    flush=[];
    flush2=[];
end


sutitl=['img ' num2str(P.imgNumAll(s)) ':' num2str(num) ' ' num2str(s)];
suptitle(sutitl);

mskL=borderFnc(sz,fBorder,ones(fliplr(P.PszXY)),b1oF,flush);
mskR=borderFnc(sz,fBorder,ones(fliplr(P.PszXY)),b1oF,flush);
r=1;
titl='Full cues';
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush);

mskL=borderFnc(sz,fBorder,~P.LmonoBG(:,:,s),b1oF,flush);
mskR=borderFnc(sz,fBorder,~P.RmonoBG(:,:,s),b1oF,flush);
r=2;
titl='No mono BG';
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush);

mskL=borderFnc(sz,fBorder,~P.LbinoBG(:,:,s),b1oF,flush);
mskR=borderFnc(sz,fBorder,~P.RbinoBG(:,:,s),b1oF,flush);
r=3;
titl='No bino BG';
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush);

mskL=borderFnc(sz,fBorder,P.LbinoFG(:,:,s),b1oF,flush);
mskR=borderFnc(sz,fBorder,P.RbinoFG(:,:,s),b1oF,flush);
r=4;
titl='No BG';
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush);

mskL=borderFnc(sz,fBorder,zeros(fliplr(P.PszXY)),b1oF,flush);
mskR=borderFnc(sz,fBorder,zeros(fliplr(P.PszXY)),b1oF,flush);
r=5;
titl='No cues';
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush,Boffset);

r=6;
mskL=borderFnc(sz,fBorder,zeros(fliplr(P.PszXY)),b1oF,flush2);
mskR=borderFnc(sz,fBorder,zeros(fliplr(P.PszXY)),b1oF,flush2);
cax=adjustPlotBoth(dims,r,P.LorRall(s),DC,rspProbeInd,rspProbe,MS,prbCur,stmL2,stmR2,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog2,flush2,[],1);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function cax=adjustPlotBoth(dims,r,LorR,DC,prbCrpInd,prbCrp,MS,prbCurXYpix,stmL,stmR,mskL,mskR,titl,cax,fBorder,rBorder,sz,flog,flush,Boffset,bRsp)
    if ~isvar('bRsp')
        bRsp=0;
    end
    if ~isvar('cax')
        cax=[];
    end
    if ~isvar('Boffset')
        Boffset=[];
    end
    if ~isvar('nsMsk')
        nsMsk=zeros(size(stmR));
        nsMsk2=zeros(size(stmR));
    end
    if bRsp
        bScatterL=1;
        bScatterR=1;
        stmL= stmL.*(mskL & ~prbCrpInd | ~flog) + (~mskL & ~prbCrpInd)*DC + prbCrp;
        stmR= stmR.*(mskR & ~prbCrpInd | ~flog) + (~mskR & ~prbCrpInd)*DC + prbCrp;
    elseif strcmp(LorR,'L')
        bScatterL=1;
        bScatterR=0;
        stmR= stmR.*(mskR | ~flog) + ~mskR*DC;
        stmL= stmL.*(mskL & ~prbCrpInd | ~flog) + (~mskL & ~prbCrpInd)*DC + prbCrp;
    elseif strcmp(LorR,'R')
        bScatterL=0;
        bScatterR=1;
        stmL= stmL.*(mskL | ~flog) + ~mskL*DC;
        stmR= stmR.*(mskR & ~prbCrpInd | ~flog) + (~mskR & ~prbCrpInd)*DC + prbCrp;
    end
    cax=adjustPlot(dims,r,1,stmL,bScatterL,MS,prbCurXYpix,titl,cax,fBorder,rBorder,sz,flush,Boffset);
    cax=adjustPlot(dims,r,2,stmR,bScatterR,MS,prbCurXYpix,titl,cax,fBorder,rBorder,sz,flush,Boffset);
end

function cax=adjustPlot(dims,r,c,stm,bScatter,MS,prbCurXYpix,titl,cax,fBorder,rBorder,sz,flush,Boffset)
    LW=2;
    if ~isvar(titl)
        titl='';
    end

    if isempty(flush)
        flushb=0;
        flushy=0;
    elseif strcmp(flush,'ET')
        flushb=-fBorder(1)*.5;
        flushy=0;
    elseif strcmp(flush,'EB')
        flushb=fBorder(1)*.5;
        flushy=fBorder(1)*.5;
    end

    ax1=subPlot(dims,r,c);
    imagesc(stm); hold on
    if bScatter
        scatterRel(prbCurXYpix(1)+fBorder(2),prbCurXYpix(2)+fBorder(1)-flushy,MS,'o','MarkerFaceColor','none','MarkerEdgeColor','w'); hold on
    end
    if isvar('Boffset')
        scatterRel(size(stm,2)/2+Boffset,prbCurXYpix(2)+fBorder(1)-flushy,MS,'o','MarkerFaceColor','none','MarkerEdgeColor','w');
    end
    if ~all(fBorder==0)
        x=fBorder(2)-rBorder(2)/3;
        y=fBorder(1)-rBorder(1)/3-flushy;
        w=sz(2)+rBorder(2)*2/3;
        h=sz(1)+rBorder(1)*2/3;
        rectangle('Position',[x,y,w,h],'EdgeColor','w','LineWidth',LW)

        SZ=size(stm)/2;
        %hori
        line([x+w x+w+rBorder(2)/4],[SZ(1) SZ(1)]-flushb/2,'Color','w','LineWidth',LW)
        line([x x-rBorder(2)/4],[SZ(1) SZ(1)]-flushb/2,'Color','w','LineWidth',LW)
        %vert
        if strcmp(flush,'EB')
            line([SZ(2) SZ(2)],[y y-rBorder(1)/2+3],'Color','w','LineWidth',LW)
            line([SZ(2) SZ(2)],[y+h y+h+rBorder(1)/4],'Color','w','LineWidth',LW)
        elseif strcmp(flush,'ET')
            line([SZ(2) SZ(2)],[y y-rBorder(1)/4],'Color','w','LineWidth',LW)
            line([SZ(2) SZ(2)],[y+h y+h+rBorder(1)/2-1],'Color','w','LineWidth',LW)
        else
            line([SZ(2) SZ(2)],[y y-rBorder(1)/4],'Color','w','LineWidth',LW)
            line([SZ(2) SZ(2)],[y+h y+h+rBorder(1)/4],'Color','w','LineWidth',LW)
        end

    end

    formatImage;
    formatFigure('','',titl);
    if ~isvar('cax')
        cax=caxis;
    else
        caxis(cax);
    end

    hold off

end

function img = borderFnc(sz,fBorder,img,bOdd,flushBorT)
    if ~isvar('bOdd')
        bOdd=0;
    end
    if ~isvar('img')
        img=ones(sz);
    end
    if ~isvar('flushBorT')
        flushBorT=[];
    end

    h1=zeros(sz(1),fBorder(2));
    if bOdd
        h2=zeros(sz(1),fBorder(2)+1);
    else
        h2=zeros(sz(1),fBorder(2));
    end
    img=horzcat(h1,img,h2);


    v1=zeros(fBorder(1),size(img,2));
    if bOdd
        v2=zeros(fBorder(1)+1,size(img,2));
    else
        v2=zeros(fBorder(1),size(img,2));
    end
    if isempty(flushBorT)
        img=vertcat(v1,img,v2);
    elseif strcmp(flushBorT,'T')
        img=vertcat(v1(1:end/2,:),v2,img);
    elseif strcmp(flushBorT,'B')
        img=vertcat(img,v1(1:end/2,:),v2);
    elseif strcmp(flushBorT,'ET')
        img=vertcat(v2,img,v1(1:end/2,:));
    elseif strcmp(flushBorT,'EB')
        img=vertcat(v1(1:end/2,:),img,v2);
    end
end
