function []=depthProbeAbove2(P,D,S,s,t,prbCurXYpix)

figure(668)

r=ceil(P.PszXY(2)/2);
c=ceil(P.PszXY(1)/2);
[X,Y]  =meshgrid(1:P.PszXY(1),1:P.PszXY(2));

%GET DEPTH OF QUERY POINT
%prbCurXYpix        = -1*S.XhatLocXYpix(t,:);

if strcmp(P.LorRall(s),'L')
  depthTrue       = interp2(X,Y,P.depthImgDisp(:,:,s),     prbCurXYpix(1),prbCurXYpix(2));
elseif strcmp(P.LorRall(s),'R')
  depthTrue       = interp2(X,Y,fliplr(P.depthImgDisp(:,:,s)),     prbCurXYpix(1)-3-P.width(s),prbCurXYpix(2));
end

%GET COORDINATES OF QUERY POINT
[LitpXYT,RitpXYT,prbTxyzM] = psyCorrespondingPointsFromDepth(depthTrue,prbCurXYpix,P.LorRall(s),P.PszXY,D.multFactorXY,D.pixPerMxy,D.scrnCtr,D.LExyz,D.RExyz,D.PPxyz);
%prbTxyzM(1)=prbTxyzM(1)*-1

%GET FGND & BCKGRND
img=P.depthImgDisp(:,:,s);
mid=P.depthImgDisp(r,:,s);
men=nanmean(img,1);
med=nanmedian(img,1);
if P.LorRall(s)=='R'
    mm=minmax(find(fliplr(P.RmonoBG(r,:,s))))+4;
elseif P.LorRall(s)=='L'
    mm=minmax(find(P.LmonoBG(r,:,s)));
end
m=repmat(ceil(P.PszXY(1)/2),1,2);

%GET x coordinates in meters
distXYm=D.CppZm.*tand(D.stmXYdeg./2);
x=linspace(-distXYm(1),distXYm(1),P.PszXY(1));
dist=abs(x(2)-x(1));

%GET FOREGROUND EDGE
if     P.LorRall(s)=='L'
  edgInd=find(diff(mid)==min(diff(mid(c-15:c+15))));
  edgeInd=edgInd(1);
  EdgInd=edgInd+1;
elseif P.LorRall(s)=='R'
  edgInd=find(diff(mid)==max(diff(mid(c-40:c+00))));
  edgeInd=edgInd(end);
  EdgInd=edgInd+1;
end
Ledg=intersectLinesFromPoints(D.LExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 1.25],[D.RExyz(1) 0 1.25]);
Redg=intersectLinesFromPoints(D.RExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 1.25],[D.RExyz(1) 0 1.25]);

%GET ALL EDGES
if     P.LorRall(s)=='L'
  edgIndAll=find(abs(diff(mid))>.01);
  EdgIndAll=[1 edgIndAll+1 length(x)];
elseif P.LorRall(s)=='R'
  edgIndAll=find(abs(diff(mid))>.01);
  EdgIndAll=[1 edgIndAll+1 length(x)];
end

%XXX
if P.LorRall(s)=='L'
  distInd=c-EdgInd(1);
elseif P.LorRall(s)=='R'
  EdgInd(1)=EdgInd(1)-1;
  distInd=c-EdgInd(1);
end
mult=dist*(distInd);
%x=x+mult;
if P.LorRall(s)=='R'
    x=x+(P.width(s))*dist;
end
Ledg=intersectLinesFromPoints(D.LExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 depthTrue],[D.RExyz(1) 0 depthTrue]);
Redg=intersectLinesFromPoints(D.RExyz,[x(EdgInd) 0 mid(EdgInd)],[D.LExyz(1) 0 depthTrue],[D.RExyz(1) 0 depthTrue]);

depth=S.Xhat(s);

%GEOMETRIC DEPTH
if P.LorRall(s)=='L'
  depthG=intersectLinesFromPoints(D.LExyz,[0 0 depthTrue],D.RExyz,Redg);
elseif P.LorRall(s)=='R'
  depthG=intersectLinesFromPoints(D.RExyz,[0 0 depthTrue],D.LExyz,Ledg);
end

%GEOMETRIC X

%%%%%%%%%%%%%%%%%%%%%%%%5
%PLOT BACKGOURND
plot([0 0]); hold on
for i = 1:length(EdgIndAll)-1
    rng=EdgIndAll(i):(EdgIndAll(i+1)-1);
    plot(x(rng),mid(rng),'k','LineWidth',2);
end

%MEAN AND MEDIAN
%var(A,0,dim
%plot(x,men,'r');
%plot(x,med,'b');

%MONO ZONE
plot([D.LExyz(1), Ledg(1)],[D.LExyz(3), Ledg(3)],'k','LineWidth',.75)
plot([D.RExyz(1), Redg(1)],[D.RExyz(3), Redg(3)],'k','LineWidth',.75)

%GEOMETRIC
if P.LorRall(s)=='L'
  plot([D.LExyz(1), 0],[0, depthTrue],'r--')
elseif P.LorRall(s)=='R'
  plot([D.RExyz(1), 0],[0, depthTrue],'r--')
end

%Center line
%plot([0 0],[0 1.25],'k--')

%SUBJECT RESPONSE
scatter(0,depth, 75,'o','MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],'LineWidth',2);
scatter(depthG(1),depthG(3),75,'o','MarkerFaceColor','r','MarkerEdgeColor','r','LineWidth',2);
scatter(prbTxyzM(1),prbTxyzM(3),75,'o','MarkerFaceColor','b','MarkerEdgeColor','r','LineWidth',2);

ylim([.95 1.21])
xlim([-.02, .02])
%titl=[P.LorRall(s) ' ' num2str(s) ': k - middle, r - mean, b - median'];
formatFigure('width (m)','distance (m)',[]);
hold off
